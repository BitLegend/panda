package main

import (
	"Panda/common/config"
	"Panda/common/constant"
	"Panda/common/engine"
	"Panda/common/exception"
	"Panda/common/logs"
	"errors"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type HandlerFunction func(*engine.Context) (interface{}, error)

func HandlerFunc(handler HandlerFunction) gin.HandlerFunc {
	f := func(ctx *gin.Context) {
		var resp config.Response

		defer func() {
			if e := recover(); e != nil {
				resp = config.Response{
					Code:    exception.ServerIllegal.(*exception.Exception).Code,
					Message: exception.ServerIllegal.(*exception.Exception).Explain,
					Data:    fmt.Sprintf("%+v", e),
				}
			}
			// 返回请求结果信息
			ctx.JSON(http.StatusOK, resp)
		}()

		context := InitContext(ctx)
		if result, err := handler(&context); err == nil {
			resp = config.Response{
				Code:    exception.Success.(*exception.Exception).Code,
				Message: exception.Success.(*exception.Exception).Explain,
				Data:    result,
			}
		} else {
			// 处理内部的异常信息
			resp = config.Response{
				Code:    err.(*exception.Exception).Code,
				Message: err.(*exception.Exception).Explain,
				Data:    nil,
			}
		}
	}
	return f
}

// InitContext 将上下文转换成系统的上下文
func InitContext(ctx *gin.Context) engine.Context {
	return handlerContext(ctx)
}

func handlerContext(ctx *gin.Context) engine.Context {
	// get log id
	logId, ok := ctx.Keys[constant.LogKey].(string)
	if !ok {
		logId = logs.GenLogID()
		ctx.Keys[constant.LogKey] = logId
	}

	// get token
	token, err := ctx.Cookie(constant.HttpTokenHeaderKey)
	if err != nil && !errors.Is(err, http.ErrNoCookie) {
		logs.Error(ctx, "")
	}

	return engine.Context{
		Context:    ctx,
		Token:      token,
		LogID:      logId,
		Session:    map[string]string{},
		ExtendInfo: map[string]string{},
	}
}
