package model

import (
	"Panda/pojo/response"
)

type Question struct {
	ID             int64    `json:"id"`
	QuestionText   string   `json:"question_text"`
	QuestionType   int      `json:"question_type"`
	QuestionAnswer string   `json:"question_answer"`
	QuestionScore  float32  `json:"question_score"`
	TopicID        int64    `json:"topic_id"`
	CreatorID      string   `json:"creator_id"`
	Options        []Option `json:"options"`
}

func (q Question) ToResponse() response.Question {
	return response.Question{
		ID:             q.ID,
		QuestionText:   q.QuestionText,
		QuestionType:   "",
		QuestionAnswer: q.QuestionAnswer,
		QuestionScore:  q.QuestionScore,
		TopicID:        q.TopicID,
		CreatorID:      q.CreatorID,
	}
}
