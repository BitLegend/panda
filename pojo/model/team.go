package model

import "Panda/pojo/response"

type Team struct {
	ID              int64  `json:"id,omitempty"`
	TeamName        string `json:"team_name,omitempty"`
	TeamIntroduce   string `json:"team_introduce,omitempty"`
	TeamMotto       string `json:"team_motto,omitempty"`
	TeamMemberCount int    `json:"team_member_count,omitempty"`
	TeamMail        string `json:"team_mail,omitempty"`
	TeamAddress     string `json:"team_address,omitempty"`
	TeamTel         string `json:"team_tel,omitempty"`
	CreatorID       string `json:"creator_id"`
}

func (t Team) ToResponse() response.Team {
	return response.Team{
		ID:              t.ID,
		TeamName:        t.TeamName,
		TeamIntroduce:   t.TeamIntroduce,
		TeamMotto:       t.TeamMotto,
		TeamMemberCount: t.TeamMemberCount,
		TeamMail:        t.TeamMail,
		TeamAddress:     t.TeamAddress,
		TeamTel:         t.TeamTel,
		CreatorID:       t.CreatorID,
	}
}
