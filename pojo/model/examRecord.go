package model

import "Panda/pojo/response"

type ExamRecord struct {
	ID           int64
	PaperID      int64
	UserID       string
	ExamIndex    int
	TopicID      int64
	QuestionID   int64
	UserAnswer   *string
	ActualAnswer *string
	RecordScore  *float64
}

func (record *ExamRecord) ToResponse() response.ExamRecord {
	return response.ExamRecord{
		ID:           record.ID,
		PaperID:      record.PaperID,
		UserID:       record.UserID,
		ExamIndex:    record.ExamIndex,
		TopicID:      record.TopicID,
		QuestionID:   record.QuestionID,
		UserAnswer:   *record.UserAnswer,
		ActualAnswer: *record.ActualAnswer,
		RecordScore:  *record.RecordScore,
	}
}
