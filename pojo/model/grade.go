package model

import "Panda/pojo/response"

type Grade struct {
	ID            int64
	UserID        string
	PaperID       int64
	ExamIndex     int
	GradeScore    float64
	CorrectCount  int
	FaultCount    int
	TopicCount    int
	ExamSpendTime int
}

func (g Grade) ToResponse() response.Grade {
	return response.Grade{
		ID:            g.ID,
		UserID:        g.UserID,
		PaperID:       g.PaperID,
		ExamIndex:     g.ExamIndex,
		GradeScore:    g.GradeScore,
		CorrectCount:  g.CorrectCount,
		FaultCount:    g.FaultCount,
		TopicCount:    g.TopicCount,
		ExamSpendTime: g.ExamSpendTime,
	}
}
