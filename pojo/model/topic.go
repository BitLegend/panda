package model

import "Panda/pojo/response"

type Topic struct {
	ID        int64      `json:"id,omitempty"`
	TopicText string     `json:"topic_text,omitempty"`
	TopicType int        `json:"topic_type,omitempty"`
	TeamID    int64      `json:"team_id,omitempty"`
	CreatorID string     `json:"creator_id"`
	Questions []Question `json:"questions"`
}

func (t Topic) ToResponse() response.Topic {
	return response.Topic{
		ID:        t.ID,
		TopicText: t.TopicText,
		TopicType: "",
		TeamID:    t.TeamID,
	}
}
