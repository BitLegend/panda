package model

import (
	"Panda/pojo/response"
	"time"
)

type Paper struct {
	ID             int64
	PaperName      string
	PaperIntroduce string
	PaperTeamID    int64
	PaperStartTime time.Time
	PaperEndTime   time.Time
	ValidTimeCount int64
	MaxJoinCount   int
	PaperPassScore float64
	PaperPassCount int
	PaperJoinCount int
	CreatorID      string
}

func (p Paper) ToResponse() response.Paper {
	return response.Paper{
		ID:             p.ID,
		PaperName:      p.PaperName,
		PaperIntroduce: p.PaperIntroduce,
		PaperTeamID:    p.PaperTeamID,
		PaperStartTime: p.PaperStartTime,
		PaperEndTime:   p.PaperEndTime,
		CreatorID:      p.CreatorID,
		ValidTimeCount: p.ValidTimeCount,
		MaxJoinCount:   p.MaxJoinCount,
		PaperPassScore: p.PaperPassScore,
		PaperPassCount: p.PaperPassCount,
		PaperJoinCount: p.PaperJoinCount,
	}
}
