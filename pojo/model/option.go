package model

import "Panda/pojo/response"

type Option struct {
	ID         int64
	OptionText string
	OptionKey  string
	QuestionID int64
	CreatorID  string
}

func (p Option) ToResponse() response.Option {
	return response.Option{
		ID:         p.ID,
		OptionText: p.OptionText,
		OptionKey:  p.OptionText,
		QuestionID: p.QuestionID,
		CreatorID:  p.CreatorID,
	}
}
