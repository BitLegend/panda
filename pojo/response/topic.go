package response

type Topic struct {
	ID        int64
	TopicText string
	TopicType string
	TeamID    int64
}
