package response

type ExamRecord struct {
	ID           int64   `json:"id,omitempty"`
	PaperID      int64   `json:"paper_id,omitempty"`
	UserID       string  `json:"user_id,omitempty"`
	ExamIndex    int     `json:"exam_index,omitempty"`
	TopicID      int64   `json:"topic_id,omitempty"`
	QuestionID   int64   `json:"question_id,omitempty"`
	UserAnswer   string  `json:"user_answer,omitempty"`
	ActualAnswer string  `json:"actual_answer,omitempty"`
	RecordScore  float64 `json:"record_score,omitempty"`
}
