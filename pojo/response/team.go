package response

type Team struct {
	ID              int64
	TeamName        string
	TeamIntroduce   string
	TeamMotto       string
	TeamMemberCount int
	TeamMail        string
	TeamAddress     string
	TeamTel         string
	CreatorID       string
}
