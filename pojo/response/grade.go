package response

type Grade struct {
	ID            int64   `json:"id,omitempty"`
	UserID        string  `json:"user_id,omitempty"`
	PaperID       int64   `json:"paper_id,omitempty"`
	ExamIndex     int     `json:"exam_index,omitempty"`
	GradeScore    float64 `json:"grade_score,omitempty"`
	CorrectCount  int     `json:"correct_count,omitempty"`
	FaultCount    int     `json:"fault_count,omitempty"`
	TopicCount    int     `json:"topic_count,omitempty"`
	ExamSpendTime int     `json:"exam_spend_time,omitempty"`
}
