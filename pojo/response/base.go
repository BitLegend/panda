package response

type PageResponse struct {
	Data      interface{}
	LineCount int64
	PageIndex int64
	PageSize  int64
}

func ToPageResponse(data interface{}, pageIndex, pageSize, lineCount int64) PageResponse {
	return PageResponse{
		Data:      data,
		LineCount: lineCount,
		PageIndex: pageIndex,
		PageSize:  pageSize,
	}
}
