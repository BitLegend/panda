package response

import "Panda/common/constant"

type User struct {
	UserID       string          `json:"user_id,omitempty"`
	UserPassword string          `json:"user_password,omitempty"`
	UserName     string          `json:"user_name,omitempty"`
	UserGender   constant.Gender `json:"user_gender,omitempty"`
	UserMail     string          `json:"user_mail,omitempty"`
	UserTel      string          `json:"user_tel,omitempty"`
	UserAddress  string          `json:"user_address,omitempty"`
	UserTeamID   int64           `json:"user_team_id,omitempty"`
	UserRole     constant.Role   `json:"user_role,omitempty"`
}
