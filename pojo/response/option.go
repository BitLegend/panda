package response

type Option struct {
	ID         int64  `json:"id,omitempty"`
	OptionText string `json:"option_text,omitempty"`
	OptionKey  string `json:"option_key,omitempty"`
	QuestionID int64  `json:"question_id,omitempty"`
	CreatorID  string `json:"creator_id"`
}
