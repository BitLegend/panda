package response

type Question struct {
	ID             int64   `json:"id,omitempty"`
	QuestionText   string  `json:"question_text,omitempty"`
	QuestionType   string  `json:"question_type,omitempty"`
	QuestionAnswer string  `json:"question_answer,omitempty"`
	QuestionScore  float32 `json:"question_score,omitempty"`
	TopicID        int64   `json:"topic_id,omitempty"`
	CreatorID      string  `json:"creator_id"`
}
