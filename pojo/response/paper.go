package response

import "time"

type Paper struct {
	ID             int64     `json:"id,omitempty"`
	PaperName      string    `json:"paper_name,omitempty"`
	PaperIntroduce string    `json:"paper_introduce,omitempty"`
	PaperTeamID    int64     `json:"paper_team_id,omitempty"`
	PaperStartTime time.Time `json:"paper_start_time"`
	PaperEndTime   time.Time `json:"paper_end_time"`
	CreatorID      string    `json:"creator_id,omitempty"`
	ValidTimeCount int64     `json:"valid_time_count,omitempty"`
	MaxJoinCount   int       `json:"max_join_count,omitempty"`
	PaperPassScore float64   `json:"paper_pass_score,omitempty"`
	PaperPassCount int       `json:"paper_pass_count,omitempty"`
	PaperJoinCount int       `json:"paper_join_count,omitempty"`
}

func (p Paper) ToResponse() {

}
