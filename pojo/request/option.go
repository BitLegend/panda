package request

import "Panda/pojo/entity"

type OptionQueryRequest struct {
	OptionID   int64
	QuestionID int64
	entity.Page
}

type OptionInsertRequest struct {
	OptionText string
	OptionKey  string
	QuestionID int64
}

type OptionUpdateRequest struct {
	OptionID   int64
	OptionText string
	OptionKey  string
}
