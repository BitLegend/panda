package request

import "Panda/pojo/entity"

type QuestionInsertRequest struct {
	QuestionText   string
	QuestionType   int
	QuestionAnswer string
	QuestionScore  float32
	TopicID        int64
}

type QuestionQueryRequest struct {
	QuestionID   int64
	TopicID      int64
	QuestionText string
	CreatorID    string
	QuestionType int
	entity.Page
}

type QuestionUpdateRequest struct {
	QuestionID     int64
	QuestionText   string
	QuestionType   int
	QuestionAnswer string
	QuestionScore  float32
}
