package request

import (
	"Panda/pojo/entity"
)

type TopicQueryReq struct {
	TopicID   int64  `json:"topic_id"`
	TopicText string `json:"topic_text"`
	CreatorID string `json:"creator_id"`
	TeamID    int64  `json:"team_id"`
	TopicType int    `json:"topic_type"`
	entity.Page
}

type TopicUpdateReq struct {
	TopicID   int64  `json:"topic_id"`
	TopicText string `json:"topic_text"`
	TopicType int    `json:"topic_type"`
}

type TopicDelReq struct {
	TopicID int64 `json:"topic_id"`
}

type TopicRequest struct {
	TopicID     int64  `json:"topic_id"`
	TopicText   string `json:"topic_text"`
	TopicType   int    `json:"topic_type"`
	TeamID      int64  `json:"team_id"`
	QuestionStr string `json:"question_str"`
}
