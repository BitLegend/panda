package request

import "Panda/pojo/entity"

type TeamQueryRequest struct {
	TeamID        int64  `json:"team_id"`
	TeamName      string `json:"team_name"`
	TeamCreatorID string `json:"team_creator_id"`
	entity.Page
}

type TeamUpdateRequest struct {
	TeamID          int64  `json:"team_id,omitempty"`
	TeamName        string `json:"team_name,omitempty"`
	TeamMail        string `json:"team_mail,omitempty"`
	TeamMotto       string `json:"team_motto,omitempty"`
	TeamAddress     string `json:"team_address,omitempty"`
	TeamIntroduce   string `json:"team_introduce,omitempty"`
	TeamTel         string `json:"team_tel,omitempty"`
	TeamMemberCount int    `json:"team_member_count,omitempty"`
}

type TeamInsertRequest struct {
	TeamID          int64  `json:"team_id,omitempty"`
	TeamName        string `json:"team_name,omitempty"`
	TeamMail        string `json:"team_mail,omitempty"`
	TeamMotto       string `json:"team_motto,omitempty"`
	TeamAddress     string `json:"team_address,omitempty"`
	TeamIntroduce   string `json:"team_introduce,omitempty"`
	TeamTel         string `json:"team_tel,omitempty"`
	TeamMemberCount int    `json:"team_member_count,omitempty"`
}
