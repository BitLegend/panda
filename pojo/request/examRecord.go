package request

import "Panda/pojo/entity"

type RecordQueryRequest struct {
	RecordID    int64
	PaperID     int64
	UserID      string
	ExamIndex   int
	RecordScore *float64
	entity.Page
}

type RecordUpdateRequest struct {
	RecordID     int64
	UserAnswer   *string
	ActualAnswer *string
	RecordScore  *float64
}

type RecordInsertRequest struct {
	RecordID     int64
	PaperID      int64
	UserID       string
	ExamIndex    int
	TopicID      int64
	QuestionID   int64
	UserAnswer   *string
	ActualAnswer *string
	RecordScore  *float64
}
