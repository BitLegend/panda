package request

import "Panda/pojo/entity"

type GradeInsertRequest struct {
	GradeID       int64
	UserID        string
	ExamIndex     int
	PaperID       int64
	GradeScore    float64
	CorrectCount  int
	FaultCount    int
	TopicCount    int
	ExamSpendTime int
}

type GradeUpdateRequest struct {
	GradeID       int64
	UserID        string
	ExamIndex     int
	PaperID       int64
	GradeScore    float64
	CorrectCount  int
	FaultCount    int
	TopicCount    int
	ExamSpendTime int
}

type GradeQueryRequest struct {
	GradeID   int64
	ExamIndex int
	UserID    string
	PaperID   int64
	TeamID    int64
	entity.Page
}
