package request

import (
	"Panda/pojo/entity"
	"time"
)

type PaperInsertRequest struct {
	PaperID        int64
	PaperName      string
	PaperIntroduce string
	PaperTeamID    int64
	PaperStartTime time.Time
	PaperEndTime   time.Time
	ValidTimeCount int64
	MaxJoinCount   int
	PaperPassScore float64
}

type PaperQueryRequest struct {
	PaperID     int64
	PaperName   string
	PaperTeamID int64
	CreatorID   string
	entity.Page
}

type PaperUpdateRequest struct {
	PaperID        int64
	PaperName      string
	PaperIntroduce string
	PaperStartTime time.Time
	PaperEndTime   time.Time
	ValidTimeCount int64
	MaxJoinCount   int
	PaperPassScore float64
}
