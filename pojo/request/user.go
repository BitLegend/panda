package request

import "Panda/pojo/entity"

type UserLoginReq struct {
	UserID       string `json:"user_id"`
	UserPassword string `json:"user_password"`
}

type UserRegisterReq struct {
	UserID       string `json:"user_id"`
	UserPassword string `json:"user_password"`
	UserMail     string `json:"user_mail"`
}

type UserQueryReq struct {
	UserID     string `json:"user_id"`
	UserGender string `json:"user_gender"`
	UserMail   string `json:"user_mail"`
	UserTeamID int64  `json:"user_team_id"`
	UserName   string `json:"user_name"`
	entity.Page
}

type UserUpdateReq struct {
	UserID      string `json:"user_id"`
	UserGender  int64  `json:"user_gender"`
	UserMail    string `json:"user_mail"`
	UserTeamID  int64  `json:"user_team_id"`
	UserName    string `json:"user_name"`
	UserAddress string `json:"user_address"`
	UserTel     string `json:"user_tel"`
	UserRole    string `json:"user_role"`
}

type UserPwdReq struct {
	UserID        string `json:"user_id"`
	UserPassword  string `json:"user_password"`
	CheckPassword string `json:"check_password"`
	OldPassword   string `json:"old_password"`
}
