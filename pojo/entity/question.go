package entity

import "Panda/pojo/model"

type Question struct {
	Base           Base
	ID             int64   `gorm:"column:id,primary_key,auto_increment"`
	QuestionText   string  `gorm:"column:question_text"`
	QuestionType   int     `gorm:"column:question_type"`
	QuestionAnswer string  `gorm:"column:question_answer"`
	QuestionScore  float32 `gorm:"column:question_score"`
	TopicID        int64   `gorm:"column:topic_id"`
	CreatorID      string  `gorm:"column:creator_id"`
}

func (q *Question) TableName() string {
	return "tb_question"
}

func (q Question) ToModel() model.Question {
	return model.Question{
		ID:             q.ID,
		QuestionText:   q.QuestionText,
		QuestionType:   q.QuestionType,
		QuestionAnswer: q.QuestionAnswer,
		QuestionScore:  q.QuestionScore,
		TopicID:        q.TopicID,
	}
}
