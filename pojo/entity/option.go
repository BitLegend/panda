package entity

import "Panda/pojo/model"

//Option 题目的选型信息
type Option struct {
	Base       Base
	ID         int64  `gorm:"column:id,primary_key,auto_increment"`
	OptionText string `gorm:"column:option_text"`
	OptionKey  string `gorm:"column:option_key"`
	QuestionID int64  `gorm:"column:question_id"`
	CreatorID  string `gorm:"column:creator_id"`
}

func (p *Option) TableName() string {
	return "tb_option"
}

func (p Option) ToModel() model.Option {
	return model.Option{
		ID:         p.ID,
		OptionText: p.OptionText,
		OptionKey:  p.OptionKey,
		QuestionID: p.QuestionID,
	}
}
