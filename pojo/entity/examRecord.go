package entity

import "Panda/pojo/model"

// ExamRecord 用户作答的记录
type ExamRecord struct {
	Base         Base
	ID           int64    `gorm:"column:id,primary_key,auto_increment"`
	PaperID      int64    `gorm:"column:paper_id"`
	UserID       string   `gorm:"column:user_id"`
	ExamIndex    int      `gorm:"column:exam_index"`
	TopicID      int64    `gorm:"column:topic_id"`
	QuestionID   int64    `gorm:"column:question_id"`
	UserAnswer   *string  `gorm:"column:user_answer"`
	ActualAnswer *string  `gorm:"column:actual_answer"`
	RecordScore  *float64 `gorm:"column:record_score"`
}

func (e *ExamRecord) TableName() string {
	return "tb_exam_record"
}

func (e ExamRecord) ToModel() model.ExamRecord {
	return model.ExamRecord{
		ID:           e.ID,
		PaperID:      e.PaperID,
		UserID:       e.UserID,
		ExamIndex:    e.ExamIndex,
		TopicID:      e.TopicID,
		QuestionID:   e.QuestionID,
		UserAnswer:   e.UserAnswer,
		ActualAnswer: e.ActualAnswer,
		RecordScore:  e.RecordScore,
	}
}
