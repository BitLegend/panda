package entity

import "Panda/pojo/model"

type Team struct {
	Base            Base
	ID              int64  `gorm:"column:id,primary_key,auto_increment"`
	TeamName        string `gorm:"column:team_name"`
	TeamIntroduce   string `gorm:"column:team_introduce"`
	TeamMotto       string `gorm:"column:team_motto"`
	TeamMemberCount int    `gorm:"column:team_member_count"`
	TeamMail        string `gorm:"column:team_mail"`
	TeamTel         string `gorm:"column:team_tel"`
	TeamAddress     string `gorm:"column:team_address"`
	CreatorID       string `gorm:"column:creator_id"`
}

func (t *Team) TableName() string {
	return "tb_team"
}

func (t Team) ToModel() model.Team {
	return model.Team{
		ID:              t.ID,
		TeamName:        t.TeamName,
		TeamIntroduce:   t.TeamIntroduce,
		TeamMotto:       t.TeamMotto,
		TeamMemberCount: t.TeamMemberCount,
		TeamMail:        t.TeamMail,
		TeamAddress:     t.TeamAddress,
		TeamTel:         t.TeamTel,
	}
}
