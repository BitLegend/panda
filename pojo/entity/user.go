package entity

import (
	"Panda/common/constant"
	"Panda/pojo/model"
)

type User struct {
	Base         Base
	ID           int64  `gorm:"column:id,primary_key,auto_increment"`
	UserID       string `gorm:"column:user_id"`
	UserPassword string `gorm:"column:user_password"`
	UserGender   int64  `gorm:"column:user_gender"`
	UserName     string `gorm:"column:user_name"`
	UserMail     string `gorm:"column:user_mail"`
	UserTel      string `gorm:"column:user_tel"`
	UserAddress  string `gorm:"column:user_address"`
	UserTeamID   int64  `gorm:"column:user_team_id"`
	UserRole     int64  `gorm:"column:user_role"`
}

func (u *User) TableName() string {
	return "tb_user"
}

func (u User) ToModel() model.User {
	return model.User{
		UserID:       u.UserID,
		UserPassword: u.UserPassword,
		UserName:     u.UserName,
		UserGender:   constant.MatchGenderCode(u.UserGender),
		UserMail:     u.UserMail,
		UserTel:      u.UserTel,
		UserAddress:  u.UserAddress,
		UserTeamID:   u.UserTeamID,
		UserRole:     constant.MatchRoleCode(u.UserRole),
	}
}
