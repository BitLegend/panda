package entity

import "time"

//Base is all the entity base struct
type Base struct {
	DeleteTime time.Time `gorm:"column:delete_time" json:"delete_time"`
	UpdateTime time.Time `gorm:"column:update_time" json:"update_time"`
	InsertTime time.Time `gorm:"column:insert_time" json:"insert_time"`
}

type Page struct {
	PageSize  int64 `json:"page_size"`
	PageIndex int64 `json:"page_index"`
	LineStart int64
}
