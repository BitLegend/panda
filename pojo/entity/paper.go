package entity

import (
	"Panda/pojo/model"
	"time"
)

type Paper struct {
	Base           Base
	ID             int64     `gorm:"column:id,primary_key,auto_increment"`
	PaperName      string    `gorm:"column:paper_name"`
	PaperTeamID    int64     `gorm:"column:paper_team_id"`
	PaperIntroduce string    `gorm:"column:paper_introduce"`
	PaperStartTime time.Time `gorm:"column:paper_start_time"`
	PaperEndTime   time.Time `gorm:"column:paper_end_time"`
	ValidTimeCount int64     `gorm:"column:valid_time_count"`
	MaxJoinCount   int       `gorm:"column:max_join_count"`
	PaperPassScore float64   `gorm:"column:paper_pass_score"`
	PaperPassCount int       `gorm:"column:paper_pass_count"`
	PaperJoinCount int       `gorm:"column:paper_join_count"`
	CreatorID      string    `gorm:"column:creator_id"`
}

func (p *Paper) TableName() string {
	return "tb_paper"
}

func (p Paper) ToModel() model.Paper {
	return model.Paper{
		ID:             p.ID,
		PaperName:      p.PaperName,
		PaperIntroduce: p.PaperIntroduce,
		PaperTeamID:    p.PaperTeamID,
		PaperStartTime: p.PaperStartTime,
		PaperEndTime:   p.PaperEndTime,
		ValidTimeCount: p.ValidTimeCount,
		MaxJoinCount:   p.MaxJoinCount,
		PaperPassScore: p.PaperPassScore,
		PaperPassCount: p.PaperPassCount,
		PaperJoinCount: p.PaperJoinCount,
		CreatorID:      p.CreatorID,
	}
}
