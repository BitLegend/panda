package entity

import "Panda/pojo/model"

// Grade 成绩信息
type Grade struct {
	Base          Base
	ID            int64   `gorm:"column:id,primary_key,auto_increment"`
	UserID        string  `gorm:"column:user_id"`
	PaperID       int64   `gorm:"column:paper_id"`
	ExamIndex     int     `gorm:"column:exam_index"`
	GradeScore    float64 `gorm:"column:grade_score"`
	CorrectCount  int     `gorm:"column:correct_count"`
	FaultCount    int     `gorm:"column:fault_count"`
	TopicCount    int     `gorm:"column:topic_count"`
	ExamSpendTime int     `gorm:"column:exam_spend_time"`
}

func (g *Grade) TableName() string {
	return "tb_grade"
}

func (g Grade) ToModel() model.Grade {
	return model.Grade{
		ID:            g.ID,
		UserID:        g.UserID,
		PaperID:       g.PaperID,
		ExamIndex:     g.ExamIndex,
		GradeScore:    g.GradeScore,
		CorrectCount:  g.CorrectCount,
		FaultCount:    g.FaultCount,
		TopicCount:    g.TopicCount,
		ExamSpendTime: g.ExamSpendTime,
	}
}
