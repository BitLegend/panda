package entity

import "Panda/pojo/model"

type Topic struct {
	Base      Base
	ID        int64  `gorm:"column:id,primary_key,auto_increment"`
	TopicText string `gorm:"column:topic_text"`
	TopicType int    `gorm:"column:topic_type"`
	TeamID    int64  `gorm:"column:team_id"`
	CreatorID string `gorm:"column:creator_id"`
}

func (t *Topic) TableName() string {
	return "tb_topic"
}

func (t *Topic) ToModel() model.Topic {
	return model.Topic{
		ID:        t.ID,
		TopicText: t.TopicText,
		TopicType: t.TopicType,
		TeamID:    t.TeamID,
	}
}
