package config

type Response struct {
	Code    int
	Message string
	Data    interface{}
}
