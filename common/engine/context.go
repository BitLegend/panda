package engine

import (
	"github.com/gin-gonic/gin"
)

type Context struct {
	*gin.Context
	LogID      string
	Token      string
	Session    map[string]string
	ExtendInfo map[string]string
	Error      error
}

func (ctx *Context) Err() error {
	return ctx.Error
}

func (ctx *Context) Value(key interface{}) interface{} {
	switch key.(type) {
	case string:
		if v, ok := ctx.ExtendInfo[key.(string)]; ok {
			return v
		} else {
			return ctx.Context.Value(key)
		}
	}
	return nil
}

func (ctx *Context) GetLogID() string {
	if ctx.LogID == "" {
		return ctx.Context.Value("log_id").(string)
	}
	return ctx.LogID
}
