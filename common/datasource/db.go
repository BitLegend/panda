package datasource

import (
	"Panda/common/setting"
	"fmt"
	"sync"

	"github.com/gin-gonic/gin"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/prometheus/common/log"
)

var (
	conn *gorm.DB

	lock = new(sync.Mutex)
)

func GetConnection() *gorm.DB {
	if conn == nil {
		lock.Lock()
		defer lock.Unlock()
		if conn == nil {
			err := InitDB()
			if err != nil {
				panic(err)
			}
		}
	}
	return conn
}

func InitDB() error {
	config := setting.AppConfig
	var err error
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local&timeout=%dms",
		config.Mysql.User, config.Mysql.Password, config.Mysql.Host, config.Mysql.Port, config.Mysql.DBName, config.Mysql.ReadTimeOut)
	conn, err = gorm.Open("mysql", dsn)
	if err != nil {
		log.Error(fmt.Sprintf("Init DB fail [dsn=%v,err=%v]", dsn, err))
		return err
	}
	conn.LogMode(true)
	gin.SetMode(gin.DebugMode)
	if config.Mysql.IdleCount > 0 {
		conn.DB().SetMaxIdleConns(config.Mysql.IdleCount)
	}
	if config.Mysql.MaxConn > 0 {
		conn.DB().SetMaxOpenConns(config.Mysql.MaxConn)
	}
	conn.SingularTable(true)
	return nil
}
