package constant

const (
	//CtxCreatorID creator id
	CtxCreatorID string = "creatorID"

	//TokenGenKey token private key
	TokenGenKey = "fcguwhd232iwfhu4tr"

	//HttpTokenHeaderKey http cookie key
	HttpTokenHeaderKey = "http_token_key"

	//LogKey global log key
	LogKey = "log_id"
)
