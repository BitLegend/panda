package constant

type GenderType int64

const (
	GenderTypeMan GenderType = 1 + iota
	GenderTypeWoman
)

type GenderText string

const (
	GenderTextMan   GenderText = "男"
	GenderTextWoman GenderText = "女"
)

type Gender struct {
	Code GenderType `json:"code"`
	Text GenderText `json:"text"`
}

var (
	GenderMan = Gender{
		Code: GenderTypeMan,
		Text: GenderTextMan,
	}
	GenderWoman = Gender{
		Code: GenderTypeWoman,
		Text: GenderTextWoman,
	}
	UnKnowGender = Gender{
		Code: -1,
		Text: "未知",
	}
)

func MatchGenderCode(code int64) Gender {
	switch code {
	case int64(GenderTypeMan):
		return GenderMan
	case int64(GenderTypeWoman):
		return GenderWoman
	}
	return UnKnowGender
}

func MatchGenderText(text string) Gender {
	switch text {
	case string(GenderTextWoman):
		return GenderWoman
	case string(GenderTextMan):
		return GenderMan
	}
	return UnKnowGender
}
