package constant

type RoleType int64

const (
	RoleTypeStudent = iota + 1
	RoleTypeTeacher
	RoleTypeAdmin
	RoleTypeSuperAdmin
)

type RoleText string

const (
	RoleTextStudent    RoleText = "学生"
	RoleTextTeacher    RoleText = "教师"
	RoleTextAdmin      RoleText = "管理员"
	RoleTextSuperAdmin RoleText = "超级管理员"
)

type Role struct {
	Text RoleText `json:"text"`
	Code RoleType `json:"code"`
}

var (
	RoleStudent = Role{
		Text: RoleTextStudent,
		Code: RoleTypeStudent,
	}

	RoleTeacher = Role{
		Text: RoleTextTeacher,
		Code: RoleTypeTeacher,
	}

	RoleAdmin = Role{
		Text: RoleTextAdmin,
		Code: RoleTypeAdmin,
	}

	RoleSuperAdmin = Role{
		Text: RoleTextSuperAdmin,
		Code: RoleTypeSuperAdmin,
	}

	UnKnowRole = Role{
		Text: "未知",
		Code: -1,
	}
)

func MatchRoleCode(code int64) Role {
	switch code {
	case int64(RoleTypeStudent):
		return RoleStudent
	case int64(RoleTypeTeacher):
		return RoleTeacher
	case int64(RoleTypeAdmin):
		return RoleAdmin
	case int64(RoleTypeSuperAdmin):
		return RoleSuperAdmin
	}
	return UnKnowRole
}

func MatchRoleText(text string) Role {
	switch text {
	case string(RoleTextStudent):
		return RoleStudent
	case string(RoleTextTeacher):
		return RoleTeacher
	case string(RoleTextAdmin):
		return RoleAdmin
	case string(RoleTextSuperAdmin):
		return RoleSuperAdmin
	}
	return UnKnowRole
}
