package setting

import (
	"os"

	"github.com/spf13/viper"
)

const ExamEnv = "Exam_Env"

var AppConfig *Config

type Config struct {
	Mysql       DataSource
	Redis       Cache
	Application Application
}

func InitAppConfig() error {
	env := os.Getenv(ExamEnv)
	if env == "" {
		env = "dev"
	}
	v := viper.New()
	v.SetConfigName(env)
	v.SetConfigType("yaml")
	v.AddConfigPath("./conf")
	v.AddConfigPath("./../conf")
	v.AddConfigPath("./../../conf")
	v.AddConfigPath("./../../../conf")

	err := v.ReadInConfig()
	if err != nil {
		return err
	}
	return v.Unmarshal(&AppConfig)
}
