package setting

type Cache struct {
	Port        int
	IP          string
	DB          int
	MaxRetry    int
	ConnTimeOut int
	IdleTimeOut int
}
