package setting

type DataSource struct {
	DBName      string
	User        string
	Host        string
	Password    string
	Port        int
	IdleTimeOut int
	ConnTimeOut int
	MaxConn     int
	IdleCount   int
	ReadTimeOut int
}
