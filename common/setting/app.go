package setting

const (
	DefaultPort = 8080
)

type Application struct {
	// run scope
	Env string
	// server run port
	Port int64
}

func (app *Application) GetPort() int64 {
	if app.Port == 0 {
		return DefaultPort
	}
	return app.Port
}
