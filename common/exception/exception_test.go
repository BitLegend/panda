package exception

import (
	"fmt"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestNewException(t *testing.T) {
	Convey("test new exception", t, func() {
		e := ExamMysqlConnIllegal
		//输出整个异常信息
		fmt.Println(e.Error())
		// 输出异常信息的结构信息
		fmt.Println(e.(*Exception).Code)
		fmt.Println(e.(*Exception).Explain)
		fmt.Println(e.(*Exception).Err)
	})
}
