package exception

import (
	"errors"
	"fmt"
)

type Exception struct {
	Code    int
	Explain string
	Err     error
}

func (e *Exception) Error() string {
	return e.Err.Error()
}

func NewException(code int, msg string) error {
	return &Exception{
		Code:    code,
		Explain: msg,
		Err:     errors.New(fmt.Sprintf("Exception.Code[%v].[%v]", code, msg)),
	}
}

var (
	Success                     = NewException(1000001, "请求成功")
	NotPermisson                = NewException(1000002, "无资访问权限")
	RequestLimit                = NewException(1000003, "请求次数超出限制，请稍后重试")
	RequestTimeOut              = NewException(1000004, "请求超时")
	RequestParamsIllegal        = NewException(1000005, "请求参数不合法")
	AuthenticationFail          = NewException(1000006, "登录信息认证失败")
	TokenGenerateFail           = NewException(1000007, "Token生成失败")
	ExamMysqlConnIllegal        = NewException(2000001, "数据库连接异常")
	MysqlUpdateIllegalException = NewException(2000002, "数据库更新请求不合法，更新条件缺失！")
	DataNotExist                = NewException(2000003, "数据不存在")
	DataExist                   = NewException(2000004, "数据已存在")
	TeamDataNotFound            = NewException(3000001, "团队信息不存在")
	MailAlreadyUsed             = NewException(3000002, "邮箱已被注册")
	MailNotActivate             = NewException(4000001, "邮箱未被激活")
	ServerIllegal               = NewException(9000001, "服务异常")
)
