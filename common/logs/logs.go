package logs

import (
	"context"
	"fmt"
	"time"
)

func GenLogID() string {
	return fmt.Sprintf("%v", time.Now().UnixNano())
}

func Debug(ctx context.Context, value interface{}, params ...interface{}) {
}

func Info(ctx context.Context, value interface{}, params ...interface{}) {
}

func Warn(ctx context.Context, value interface{}, params ...interface{}) {
}

func Error(ctx context.Context, value interface{}, params ...interface{}) {
}

func Panic(ctx context.Context, value interface{}, params ...interface{}) {
}
