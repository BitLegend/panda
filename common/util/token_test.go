package util

import (
	"Panda/common/constant"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

const example = "eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2Mzg2ODg5NzcsImp0aSI6IntcInVzZXJfaWRcIjpcIjIwMTcxOTA1XCIsXCJ1c2VyX3Bhc3N3b3JkXCI6XCIxMjM0NTZcIixcInVzZXJfbmFtZVwiOlwidmVjdG9yXCIsXCJ1c2VyX2dlbmRlclwiOntcImNvZGVcIjoxLFwidGV4dFwiOlwi55S3XCJ9LFwidXNlcl9tYWlsXCI6XCJ2ZWN0b3JAcXEuY29tXCIsXCJ1c2VyX3RlbFwiOlwiMTIzXCIsXCJ1c2VyX2FkZHJlc3NcIjpcIm5vbmVcIixcInVzZXJfcm9sZVwiOntcInRleHRcIjpcIuWtpueUn1wiLFwiY29kZVwiOjF9LFwibG9naW5fdG9rZW5cIjpcIlwifSIsImlhdCI6MTYzODY4MTc3NywiaXNzIjoiY29vbGNhci9hdXRoIn0"

func TestGetToken(t *testing.T) {
	Convey("TestGetToken", t, func() {
	})
}

func TestDecodeToken(t *testing.T) {
	Convey("TestDecodeToken", t, func() {
		token, err := GetToken("hello world", constant.TokenGenKey, 2*time.Hour)
		So(err, ShouldBeNil)
		data, err := DecodeToken(token, constant.TokenGenKey)
		t.Logf("%v", data)
		So(err, ShouldBeNil)
	})
}
