package util

import (
	"encoding/json"
	"time"

	"github.com/dgrijalva/jwt-go"
)

func GetToken(params interface{}, privateKey string, expireTime time.Duration) (string, error) {
	bytes, err := json.Marshal(params)
	if err != nil {
		return "", err
	}

	now := time.Now().Unix()
	cli := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		ExpiresAt: now + int64(expireTime.Seconds()),
		Id:        string(bytes),
		IssuedAt:  now,
		Issuer:    "vector",
	})

	return cli.SignedString([]byte(privateKey))
}

func DecodeToken(tokenStr string, privateKey string) (string, error) {
	token, err := jwt.ParseWithClaims(tokenStr, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(privateKey), nil
	})

	if err != nil {
		return "", err
	}

	return token.Claims.(*jwt.StandardClaims).Id, nil
}
