package proxy

import (
	"Panda/common/exception"
	"Panda/pojo/entity"
	"time"

	"github.com/jinzhu/gorm"
)

type GradeProxy struct {
}

//InsertGrade 插入用户的作答成绩信息
func (proxy *GradeProxy) InsertGrade(db *gorm.DB, grade entity.Grade) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	err := db.Model(&grade).Create(&grade).Error
	return grade.ID, err
}

//UpdateGradeByCond 更新用户的作答成绩信息，耗时、正确数、错误数、答题数、分数
func (proxy *GradeProxy) UpdateGradeByCond(db *gorm.DB, query entity.Grade) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.UserID != "" {
		db = db.Where("user_id = ?", query.UserID)
	}
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	if query.ExamIndex != 0 {
		db = db.Where("exam_index = ?", query.ExamIndex)
	}
	if query.PaperID != 0 {
		db = db.Where("paper_id = ?", query.PaperID)
	}
	old := entity.Grade{
		CorrectCount: query.CorrectCount,
		FaultCount:   query.FaultCount,
		TopicCount:   query.TopicCount,
	}
	if query.ExamSpendTime != 0 {
		old.ExamSpendTime = query.ExamSpendTime
	}
	if query.GradeScore != 0 {
		old.GradeScore = query.GradeScore
	}
	res := db.Update(&old,
		gorm.Expr("correct_count + ?", old.CorrectCount),
		gorm.Expr("fault_count + ?", old.FaultCount),
		gorm.Expr("topic_count + ?", old.TopicCount))
	return res.RowsAffected, res.Error
}

// DeleteGradeByCond 根据条件删除成绩信息
func (proxy *GradeProxy) DeleteGradeByCond(db *gorm.DB, query entity.Grade) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}

	db = db.Model(query).Where("delete_time IS NULL")
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
		return 0, db.Update("delete_time", time.Now()).Error
	}
	if query.PaperID != 0 {
		db = db.Where("paper_id = ?", query.PaperID)
	}
	if query.GradeScore != 0 {
		db = db.Where("grade_score = ?", query.GradeScore)
	}
	if query.ExamIndex != 0 {
		db = db.Where("exam_index = ?", query.ExamIndex)
	}
	if query.TopicCount != 0 {
		db = db.Where("topic_count = ? ", query.TopicCount)
	}

	res := db.Update("delete_time", time.Now())
	return res.RowsAffected, res.Error
}

// SelectGradeByCond 根据条件分页查询成绩信息，ID、试卷ID、考试下标
func (proxy *GradeProxy) SelectGradeByCond(db *gorm.DB, query entity.Grade, page *entity.Page) ([]entity.Grade, error) {
	if db == nil {
		return make([]entity.Grade, 0, 0), exception.ExamMysqlConnIllegal
	}

	db = db.Model(query).Where("delete_time IS NULL")

	if page == nil {
		page = &entity.Page{
			PageSize:  1,
			LineStart: 0,
		}
	}

	if page.PageSize != 0 {
		db.Limit(page.PageSize)
	}
	if page.LineStart >= 0 {
		db.Offset(page.LineStart)
	}

	result := make([]entity.Grade, 0, page.PageSize)

	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
		err := db.Find(&result).Error
		if err != nil {
			return make([]entity.Grade, 0, 0), err
		}
		return result, nil
	}

	if query.PaperID != 0 {
		db = db.Where("paper_id = ?", query.PaperID)
	}
	if query.ExamIndex != 0 {
		db = db.Where("exam_index = ?", query.ExamIndex)
	}
	if query.UserID != "" {
		db = db.Where("user_id = ?", query.UserID)
	}

	err := db.Find(&result).Error
	if err != nil {
		return make([]entity.Grade, 0, 0), err
	}
	return result, nil
}

// CountGradeByCond 根据条件查询成绩的数量
func (proxy *GradeProxy) CountGradeByCond(db *gorm.DB, query entity.Grade) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}

	db = db.Model(query).Where("delete_time IS NULL")
	if query.PaperID != 0 {
		db = db.Where("paper_id = ?", query.PaperID)
	}
	if query.ExamIndex != 0 {
		db = db.Where("exam_index = ?", query.ExamIndex)
	}
	if query.UserID != "" {
		db = db.Where("user_id = ?", query.UserID)
	}

	count := int64(0)
	err := db.Count(&count).Error
	if err != nil {
		return 0, err
	}
	return count, nil
}
