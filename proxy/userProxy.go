package proxy

import (
	"Panda/common/exception"
	"Panda/pojo/entity"
	"time"

	"github.com/jinzhu/gorm"
)

type UserProxy struct {
}

// InsertUser 插入一个用户的信息
func (proxy *UserProxy) InsertUser(db *gorm.DB, user entity.User) error {
	if db == nil {
		return exception.ExamMysqlConnIllegal
	}
	return db.Model(user).Create(&user).Error
}

// UpdateUserByCond 根据条件更新学生的信息
func (proxy *UserProxy) UpdateUserByCond(db *gorm.DB, query entity.User) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}

	user := entity.User{}
	if query.UserRole != 0 {
		user.UserRole = query.UserRole
	}
	if query.UserAddress != "" {
		user.UserAddress = query.UserAddress
	}
	if query.UserMail != "" {
		user.UserMail = query.UserMail
	}
	if query.UserGender != 0 {
		user.UserGender = query.UserGender
	}
	if query.UserTel != "" {
		user.UserTel = query.UserTel
	}
	r := db.Model(query).Where("delete_time IS NULL").Where("user_id = ?", user.UserID).Update(&user)
	return r.RowsAffected, r.Error
}

// UpdateUserPwd 更新用户的密码信息
func (proxy *UserProxy) UpdateUserPwd(db *gorm.DB, old entity.User) error {
	if db == nil {
		return exception.ExamMysqlConnIllegal
	}

	user := entity.User{
		UserPassword: old.UserPassword,
	}

	return db.Model(old).Where("delete_time IS NULL").Where("user_id = ?", old.UserID).Update(&user).Error
}

// DeleteUserByCond 根据条件删除用户的信息
func (proxy *UserProxy) DeleteUserByCond(db *gorm.DB, query entity.User) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	if query.UserID != "" {
		db = db.Where("user_id = ?", query.UserID)
	}
	if query.UserTeamID != 0 {
		db = db.Where("user_team_id = ?", query.UserTeamID)
	}
	res := db.Update("delete_time", time.Now())
	return res.RowsAffected, res.Error
}

// SelectUserByCond 根据条件查询用户的信息，不需要分页指定分页查询字段page为nil
func (proxy *UserProxy) SelectUserByCond(db *gorm.DB, user entity.User, page *entity.Page) ([]entity.User, error) {
	if db == nil {
		return make([]entity.User, 0, 0), exception.ExamMysqlConnIllegal
	}

	if page == nil {
		page = &entity.Page{
			PageSize:  1,
			LineStart: 0,
		}
	}

	if page.PageSize != 0 {
		db.Limit(page.PageSize)
	}
	if page.LineStart >= 0 {
		db.Offset(page.LineStart)
	}

	db.Model(user).Where("delete_time IS NULL")
	if user.UserID != "" || user.ID != 0 {
		users := make([]entity.User, 0, 1)
		if user.ID != 0 {
			db.Where("id = ?", user.ID)
		} else if user.UserID != "" {
			db.Where("user_id = ?", user.UserID)
		}
		err := db.Find(&users).Error
		if err != nil {
			return make([]entity.User, 0, page.PageSize), err
		}
		return users, nil
	}
	users := make([]entity.User, 0, page.PageSize)
	if user.UserName != "" {
		db = db.Where("user_name LIKE CONCAT('%',?)", user.UserName)
	}
	if user.UserGender != 0 {
		db = db.Where("user_gender = ?", user.UserGender)
	}
	if user.UserMail != "" {
		db = db.Where("user_mail LIKE CONCAT('%',?)", user.UserMail)
	}
	if user.UserRole != 0 {
		db = db.Where("user_role = ?", user.UserRole)
	}
	if user.UserTeamID != 0 {
		db = db.Where("user_team_id = ?", user.UserTeamID)
	}
	err := db.Find(&users).Error
	if err != nil {
		return make([]entity.User, 0, 0), err
	}
	return users, nil
}

// CountUserByCond 根据条件查询用户的数量
func (proxy *UserProxy) CountUserByCond(db *gorm.DB, query entity.User) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.UserID != "" {
		db = db.Where("user_id = ?", query.UserID)
	}
	if query.UserTeamID != 0 {
		db = db.Where("user_team_id = ?", query.UserTeamID)
	}
	if query.UserMail != "" {
		db = db.Where("user_mail = ?", query.UserMail)
	}
	if query.UserRole != 0 {
		db = db.Where("user_role = ?", query.UserRole)
	}
	count := int64(0)
	err := db.Count(&count).Error
	return count, err
}
