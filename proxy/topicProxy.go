package proxy

import (
	"Panda/common/exception"
	"Panda/pojo/entity"
	"time"

	"github.com/jinzhu/gorm"
)

type TopicProxy struct {
}

// InsertTopic 插入一个题目信息
func (proxy *TopicProxy) InsertTopic(db *gorm.DB, topic entity.Topic) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	err := db.Model(topic).Create(&topic).Error
	if err != nil {
		return 0, err
	}
	return topic.ID, nil
}

// UpdateTopicByCond 根据条件更新题目的信息
func (proxy *TopicProxy) UpdateTopicByCond(db *gorm.DB, query entity.Topic) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.ID != 0 {
		res := db.Where("id = ?", query.ID).Update(&query)
		return res.RowsAffected, res.Error
	}
	return 0, exception.MysqlUpdateIllegalException
}

// SelectTopicByCond 根据条件查询题目的信息
func (proxy *TopicProxy) SelectTopicByCond(db *gorm.DB, query entity.Topic, page *entity.Page) ([]entity.Topic, error) {
	if db == nil {
		return make([]entity.Topic, 0, 0), exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if page == nil {
		page = &entity.Page{
			PageSize:  1,
			LineStart: 0,
		}
	}

	if page.PageSize != 0 {
		db.Limit(page.PageSize)
	}
	if page.LineStart >= 0 {
		db.Offset(page.LineStart)
	}

	if query.TopicText != "" {
		db = db.Where("topic_text LIKE CONCAT(?,'%')", query.TopicText)
	}
	if query.TopicType != 0 {
		db = db.Where("topic_type = ?", query.TopicType)
	}
	result := make([]entity.Topic, 0, page.PageSize)
	err := db.Find(&result).Error
	if err != nil {
		return make([]entity.Topic, 0, 0), err
	}
	return result, nil
}

// DeleteTopicByCond 根据条件删除题目信息
func (proxy *TopicProxy) DeleteTopicByCond(db *gorm.DB, query entity.Topic) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
		err := db.Update("delete_time", time.Now()).Error
		if err != nil {
			return 0, err
		}
		return 0, nil
	}
	if query.TopicType != 0 {
		db = db.Where("topic_type = ?", query.TopicType)
	}
	res := db.Update("delete_time", time.Now())
	return res.RowsAffected, res.Error
}

// CountTopicByCond 根据条件统计题目数量
func (proxy *TopicProxy) CountTopicByCond(db *gorm.DB, query entity.Topic) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}

	db = db.Model(query).Where("delete_time IS NULL")
	if query.TopicType != 0 {
		db = db.Where("topic_type = ?", query.TopicType)
	}
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	count := int64(0)
	err := db.Count(&count).Error
	if err != nil {
		return 0, err
	}
	return count, nil
}
