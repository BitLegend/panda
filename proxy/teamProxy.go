package proxy

import (
	"Panda/common/exception"
	"Panda/pojo/entity"
	"time"

	"github.com/jinzhu/gorm"
)

type TeamProxy struct {
}

// InsertTeam 插入一个团队信息
func (t *TeamProxy) InsertTeam(db *gorm.DB, team entity.Team) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	err := db.Model(team).Create(&team).Error
	if err != nil {
		return 0, err
	}
	return team.ID, nil
}

// UpdateTeamByCond 根据条件更新团队的信息
func (t *TeamProxy) UpdateTeamByCond(db *gorm.DB, query entity.Team) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	} else {
		if query.TeamName != "" {
			db = db.Where("team_name = ?", query.TeamName)
		}
		if query.TeamMail != "" {
			db = db.Where("team_mail = ?", query.TeamMail)
		}
	}
	res := db.Update(&query)
	return res.RowsAffected, res.Error
}

// DeleteTeamByCond 根据条件删除团队信息
func (t *TeamProxy) DeleteTeamByCond(db *gorm.DB, query entity.Team) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
		res := db.Update("delete_time", time.Now())
		return res.RowsAffected, res.Error
	} else {
		return 0, exception.ExamMysqlConnIllegal
	}
}

// SelectTeamByCond 根据条件查询团队信息
func (t *TeamProxy) SelectTeamByCond(db *gorm.DB, query entity.Team, page *entity.Page) ([]entity.Team, error) {
	if db == nil {
		return make([]entity.Team, 0, 0), exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")

	if page == nil {
		page = &entity.Page{
			PageSize:  1,
			LineStart: 0,
		}
	}

	if page.PageSize != 0 {
		db.Limit(page.PageSize)
	}
	if page.LineStart >= 0 {
		db.Offset(page.LineStart)
	}

	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	} else {
		if query.TeamName != "" {
			db = db.Where("team_name LIKE CONCAT(?,'%')", query.TeamName)
		}
		if query.TeamMail != "" {
			db = db.Where("team_mail LIKE CONCAT(?,'%')", query.TeamMail)
		}
		if query.TeamTel != "" {
			db = db.Where("team_tel LIKE CONCAT(?,'%')", query.TeamTel)
		}
	}
	result := make([]entity.Team, 0, page.PageSize)
	err := db.Find(&result).Error
	if err != nil {
		return make([]entity.Team, 0, 0), err
	}
	return result, nil
}

// CountTeamByCond 根据条件统计团队数量
func (t *TeamProxy) CountTeamByCond(db *gorm.DB, query entity.Team) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.TeamName != "" {
		db = db.Where("team_name LIKE CONCAT(?,'%')", query.TeamName)
	}
	if query.TeamMail != "" {
		db = db.Where("team_mail LIKE CONCAT(?,'%')", query.TeamMail)
	}
	if query.TeamTel != "" {
		db = db.Where("team_tel LIKE CONCAT(?,'%')", query.TeamTel)
	}
	count := int64(0)
	err := db.Count(&count).Error
	return count, err
}
