package proxy

import (
	"Panda/common/exception"
	"Panda/pojo/entity"
	"time"

	"github.com/jinzhu/gorm"
)

type OptionProxy struct {
}

// InsertOption 插入新的选项信息
func (proxy *OptionProxy) InsertOption(db *gorm.DB, option entity.Option) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	err := db.Create(&option).Error
	if err != nil {
		return 0, err
	}
	return option.ID, nil
}

// UpdateOptionByCond 更新选项的文本信息
func (proxy *OptionProxy) UpdateOptionByCond(db *gorm.DB, query entity.Option) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	if query.ID != 0 {
		db = db.Model(query).Where("delete_time IS NULL").Where("id = ?", query.ID)
		res := db.Update(&query)
		return res.RowsAffected, res.Error
	}
	return 0, exception.MysqlUpdateIllegalException
}

// SelectOptionByCond 根据特定的条件分页查询选项信息
func (proxy *OptionProxy) SelectOptionByCond(db *gorm.DB, query entity.Option, page *entity.Page) ([]entity.Option, error) {
	if db == nil {
		return make([]entity.Option, 0, 0), exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")

	if page == nil {
		page = &entity.Page{
			PageSize:  1,
			LineStart: 0,
		}
	}

	if page.PageSize != 0 {
		db.Limit(page.PageSize)
	}
	if page.LineStart >= 0 {
		db.Offset(page.LineStart)
	}

	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	if query.QuestionID != 0 {
		db = db.Where("question_id = ?", query.QuestionID)
	}
	if query.OptionKey != "" {
		db = db.Where("option_key = ?", query.OptionKey)
	}
	result := make([]entity.Option, 0, page.PageSize)
	err := db.Find(&result).Error
	if err != nil {
		return make([]entity.Option, 0, 0), err
	}
	return result, nil
}

// DeleteOptionByCond 根据指定的条件删除选项信息
func (proxy *OptionProxy) DeleteOptionByCond(db *gorm.DB, query entity.Option) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	if query.QuestionID != 0 {
		db = db.Where("question_id = ?", query.QuestionID)
	}
	if query.OptionText != "" {
		db = db.Where("option_key = ?", query.OptionKey)
	}
	res := db.Update("delete_time", time.Now())
	return res.RowsAffected, res.Error
}

// CountOptionByCond 根据指定的条件统计选项数据
func (proxy *OptionProxy) CountOptionByCond(db *gorm.DB, query entity.Option) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.QuestionID != 0 {
		db = db.Where("question_id = ?", query.QuestionID)
	}
	if query.OptionKey != "" {
		db = db.Where("option_key = ?", query.OptionKey)
	}
	count := int64(0)
	err := db.Count(&count).Error
	return count, err
}
