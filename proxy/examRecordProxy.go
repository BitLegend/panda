package proxy

import (
	"Panda/common/exception"
	"Panda/pojo/entity"
	"time"

	"github.com/jinzhu/gorm"
)

type ExamRecordProxy struct {
}

//InsertExamRecord 插入用户的考试记录
func (proxy *ExamRecordProxy) InsertExamRecord(db *gorm.DB, examRecord entity.ExamRecord) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	err := db.Model(&examRecord).Create(&examRecord).Error
	if err != nil {
		return 0, err
	}
	return examRecord.ID, nil
}

//UpdateExamRecordByCond 更新用户的考试记录信息，只更新用户答案、正确答案、获得的分数.
func (proxy *ExamRecordProxy) UpdateExamRecordByCond(db *gorm.DB, query entity.ExamRecord) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(&query).Where("delete_time IS NULL")
	if query.QuestionID != 0 {
		db = db.Where("question_id = ?", query.QuestionID)
	}
	if query.TopicID != 0 {
		db = db.Where("topic_id = ?", query.TopicID)
	}
	if query.ExamIndex != 0 {
		db = db.Where("exam_index = ?", query.ExamIndex)
	}
	if query.PaperID != 0 {
		db = db.Where("paper_id = ?", query.PaperID)
	}
	old := entity.ExamRecord{
		UserAnswer:   query.UserAnswer,
		ActualAnswer: query.ActualAnswer,
		RecordScore:  query.RecordScore,
	}
	res := db.Update(&old)
	return res.RowsAffected, res.Error
}

//DeleteExamRecordByCond 删除条件匹配的考试记录信息
func (proxy *ExamRecordProxy) DeleteExamRecordByCond(db *gorm.DB, query entity.ExamRecord) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(&query).Where("delete_time IS NULL")
	if query.QuestionID != 0 {
		db = db.Where("question_id = ?", query.QuestionID)
	}
	if query.TopicID != 0 {
		db = db.Where("topic_id = ?", query.TopicID)
	}
	if query.ExamIndex != 0 {
		db = db.Where("exam_index = ?", query.ExamIndex)
	}
	if query.PaperID != 0 {
		db = db.Where("paper_id = ?", query.PaperID)
	}
	res := db.Update("delete_time", time.Now())
	return res.RowsAffected, res.Error
}

//SelectExamRecordByCond 分页查询考试记录信息
func (proxy *ExamRecordProxy) SelectExamRecordByCond(db *gorm.DB, query entity.ExamRecord, page *entity.Page) ([]entity.ExamRecord, error) {
	if db == nil {
		return make([]entity.ExamRecord, 0, 0), exception.ExamMysqlConnIllegal
	}
	db = db.Model(&query).Where("delete_time IS NULL")

	if page == nil {
		page = &entity.Page{
			PageSize:  1,
			LineStart: 0,
		}
	}

	if page.PageSize != 0 {
		db.Limit(page.PageSize)
	}
	if page.LineStart >= 0 {
		db.Offset(page.LineStart)
	}

	result := make([]entity.ExamRecord, 0, 0)
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	if query.TopicID != 0 {
		db = db.Where("topic_id = ?", query.TopicID)
	}
	if query.UserID != "" {
		db = db.Where("user_id = ?", query.UserID)
	}
	if query.PaperID != 0 {
		db = db.Where("paper_id = ?", query.PaperID)
	}
	if query.ExamIndex != 0 {
		db = db.Where("exam_index = ?", query.ExamIndex)
	}
	err := db.Find(&result).Error
	return result, err
}

//CountExamRecordByCond 统计考试记录的数量
func (proxy *ExamRecordProxy) CountExamRecordByCond(db *gorm.DB, query entity.ExamRecord) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(&query).Where("delete_time IS NULL")
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	if query.TopicID != 0 {
		db = db.Where("topic_id = ?", query.TopicID)
	}
	if query.UserID != "" {
		db = db.Where("user_id = ?", query.UserID)
	}
	if query.PaperID != 0 {
		db = db.Where("paper_id = ?", query.PaperID)
	}
	if query.ExamIndex != 0 {
		db = db.Where("exam_index = ?", query.ExamIndex)
	}
	count := int64(0)
	err := db.Count(&count).Error
	return count, err
}
