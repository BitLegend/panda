package proxy

import (
	"Panda/common/exception"
	"Panda/pojo/entity"
	"time"

	"github.com/jinzhu/gorm"
)

type PaperProxy struct {
}

// InsertPaper 插入试卷信息
func (proxy *PaperProxy) InsertPaper(db *gorm.DB, paper entity.Paper) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	err := db.Model(paper).Create(&paper).Error
	if err != nil {
		return 0, err
	}
	return paper.ID, nil
}

// UpdatePaperByCond 根据条件更新试卷的信息
func (proxy *PaperProxy) UpdatePaperByCond(db *gorm.DB, query entity.Paper) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db.Model(query).Where("delete_time IS NULL")
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	if query.PaperTeamID != 0 {
		db = db.Where("paper_team_id = ?", query.PaperTeamID)
	}
	res := db.Update(query)
	return res.RowsAffected, res.Error
}

// DeletePaperByCond 根据条件删除试卷信息
func (proxy *PaperProxy) DeletePaperByCond(db *gorm.DB, query entity.Paper) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	if query.PaperTeamID != 0 {
		db = db.Where("paper_team_id = ?", query.PaperTeamID)
	}
	res := db.Update("delete_time", time.Now())
	return res.RowsAffected, res.Error
}

// SelectPaperByCond 根据条件查询试卷信息
func (proxy *PaperProxy) SelectPaperByCond(db *gorm.DB, query entity.Paper, page *entity.Page) ([]entity.Paper, error) {
	if db == nil {
		return make([]entity.Paper, 0, 0), exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")

	if page == nil {
		page = &entity.Page{
			PageSize:  1,
			LineStart: 0,
		}
	}

	if page.PageSize != 0 {
		db.Limit(page.PageSize)
	}
	if page.LineStart >= 0 {
		db.Offset(page.LineStart)
	}

	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	if query.PaperTeamID != 0 {
		db = db.Where("paper_team_id = ?", query.PaperTeamID)
	}
	if query.PaperName != "" {
		db = db.Where("paper_name LIKE CONCAT(?,'%')", query.PaperName)
	}
	result := make([]entity.Paper, 0, 0)
	err := db.Find(&result).Error
	if err != nil {
		return make([]entity.Paper, 0, 0), err
	}
	return result, nil
}

// CountPaperByCond 根据条件统计试卷的信息
func (proxy *PaperProxy) CountPaperByCond(db *gorm.DB, query entity.Paper) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	if query.PaperTeamID != 0 {
		db = db.Where("paper_team_id = ?", query.PaperTeamID)
	}
	if query.PaperName != "" {
		db = db.Where("paper_name LIKE CONCAT(?,'%')", query.PaperName)
	}
	count := int64(0)
	err := db.Count(&count).Error
	if err != nil {
		return 0, err
	}
	return count, nil
}
