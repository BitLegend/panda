package proxy

import (
	"Panda/common/exception"
	"Panda/pojo/entity"
	"time"

	"github.com/jinzhu/gorm"
)

type QuestionProxy struct {
}

// InsertQuestion 插入一个问题信息
func (proxy *QuestionProxy) InsertQuestion(db *gorm.DB, question entity.Question) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	err := db.Model(question).Create(question).Error
	if err != nil {
		return 0, err
	}
	return question.ID, nil
}

// UpdateQuestionCond 根据条件更新问题信息
func (proxy *QuestionProxy) UpdateQuestionCond(db *gorm.DB, query entity.Question) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	if query.QuestionType != 0 {
		db = db.Where("question_type = ?", query.QuestionType)
	}
	res := db.Update(query)
	return res.RowsAffected, res.Error
}

// DeleteQuestionByCond 根据条件删除问题信息
func (proxy *QuestionProxy) DeleteQuestionByCond(db *gorm.DB, query entity.Question) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	if query.TopicID != 0 {
		db = db.Where("topic_id = ?", query.TopicID)
	}
	if query.QuestionType != 0 {
		db = db.Where("question_type = ?", query.QuestionType)
	}
	res := db.Update("delete_time", time.Now())
	return res.RowsAffected, res.Error
}

// SelectQuestionByCond 根据条件查询问题信息
func (proxy *QuestionProxy) SelectQuestionByCond(db *gorm.DB, query entity.Question, page *entity.Page) ([]entity.Question, error) {
	if db == nil {
		return make([]entity.Question, 0, 0), exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")

	if page == nil {
		page = &entity.Page{
			PageSize:  1,
			LineStart: 0,
		}
	}

	if page.PageSize != 0 {
		db.Limit(page.PageSize)
	}
	if page.LineStart >= 0 {
		db.Offset(page.LineStart)
	}

	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	if query.QuestionType != 0 {
		db = db.Where("question_type = ?", query.QuestionType)
	}
	if query.TopicID != 0 {
		db = db.Where("topic_id = ?", query.TopicID)
	}
	result := make([]entity.Question, 0, 0)
	err := db.Find(&result).Error
	if err != nil {
		return make([]entity.Question, 0, 0), err
	}
	return result, nil
}

// CountQuestionByCond 根据条件统计问题信息
func (proxy *QuestionProxy) CountQuestionByCond(db *gorm.DB, query entity.Question) (int64, error) {
	if db == nil {
		return 0, exception.ExamMysqlConnIllegal
	}
	db = db.Model(query).Where("delete_time IS NULL")
	if query.ID != 0 {
		db = db.Where("id = ?", query.ID)
	}
	if query.QuestionType != 0 {
		db = db.Where("question_type = ?", query.QuestionType)
	}
	if query.TopicID != 0 {
		db = db.Where("topic_id = ?", query.TopicID)
	}
	count := int64(0)
	err := db.Count(&count).Error
	return count, err
}
