create database db_panda;

use db_panda;
create table tb_user
(
    id            bigint auto_increment primary key comment '唯一ID',
    user_id       char(20)  not null default '' comment '账户ID',
    user_password char(20)  not null default '' comment '用户密码',
    user_gender   char(1)   not null default 'M' comment '性别',
    user_name     char(20)  not null default '' comment '名称',
    user_mail     char(30)  not null default '' comment '邮箱',
    user_tel      char(20)  not null default '' comment '电话',
    user_address  char(50)  not null default '' comment '地址',
    user_team_id  bigint    not null comment '团队ID',
    user_role     int       not null comment '角色',
    insert_time   timestamp not null comment '插入时间',
    update_time   timestamp not null default current_timestamp comment '更新时间',
    delete_time   datetime  not null comment '删除时间',
    index index_id (id),
    index index_name_mail (user_name, user_mail)
) charset utf8;


create table tb_topic
(
    id          bigint primary key auto_increment comment '题目ID',
    topic_text  text comment '题目内容',
    topic_type  int       not null default 0 comment '题目类型',
    team_id     bigint    not null default 0 comment '团队ID',
    creator_id  char(20)  not null default '' comment '创建者',
    insert_time timestamp not null comment '插入时间',
    update_time timestamp not null default current_timestamp comment '更新时间',
    delete_time datetime  not null comment '删除时间',
    index index_id (id),
    index index_type (topic_type)
) charset utf8;

create table tb_team
(
    id                bigint primary key auto_increment comment '团队ID',
    team_name         char(30)  not null default '' comment '名称',
    team_introduce    text      not null comment '介绍',
    team_motto        char(200) not null default '' comment '格言',
    team_member_count int       not null default 0 comment '成员数量',
    team_mail         char(40)  not null default '' comment '邮箱',
    team_tel          char(20)  not null default '' comment '电话',
    team_address      char(100) not null default '' comment '地址',
    creator_id        char(20)  not null default '' comment '创建者',
    insert_time       timestamp not null comment '插入时间',
    update_time       timestamp not null default current_timestamp comment '更新时间',
    delete_time       datetime  not null comment '删除时间'
) charset utf8;


create table tb_question
(
    id              bigint primary key auto_increment comment '问题ID',
    question_text   text      not null comment '文本',
    question_type   int       not null default 0 comment '类型',
    question_answer text      not null comment '答案',
    question_score  double    not null default 0 comment '分数',
    topic_id        bigint    not null default 0 comment '题目ID',
    creator_id      char(20)  not null default 0 comment '创建者',
    insert_time     timestamp not null comment '插入时间',
    update_time     timestamp not null default current_timestamp comment '更新时间',
    delete_time     datetime  not null comment '删除时间',
    index index_question_type (question_type),
    index index_question_topic (topic_id)
) charset utf8;

create table tb_paper
(
    id               bigint primary key auto_increment comment '试卷ID',
    paper_name       char(100) not null default '' comment '名称',
    paper_team_id    bigint    not null comment '团队ID',
    paper_introduce  text      not null comment '试卷介绍',
    paper_start_time datetime  not null comment '开始时间',
    paper_end_time   datetime  not null comment '结束时间',
    valid_time_count int       not null default 0 comment '有效时间',
    max_join_count   int       not null default 0 comment '答题次数',
    paper_pass_score double    not null default 0 comment '通过分数',
    paper_pass_count int       not null default 0 comment '通过数量',
    paper_join_count int       not null default 0 comment '参加数量',
    creator_id       char(20)  not null comment '创建者',
    insert_time      timestamp not null comment '插入时间',
    update_time      timestamp not null default current_timestamp comment '更新时间',
    delete_time      datetime  not null comment '删除时间',
    index index_paper_team (paper_team_id)
) charset utf8;

create table tb_option
(
    id          bigint primary key auto_increment comment '选项ID',
    option_text text      not null comment '选项文本',
    option_key  char(10)  not null comment '选项KEY',
    question_id bigint    not null comment '问题ID',
    creator_id  char(20)  not null comment '创建者',
    insert_time timestamp not null comment '插入时间',
    update_time timestamp not null default current_timestamp comment '更新时间',
    delete_time datetime  not null comment '删除时间',
    index index_option_key (option_key)
) charset utf8;


create table tb_grade
(
    id              bigint primary key auto_increment comment '成绩ID',
    user_id         char(10)  not null comment '账户ID',
    paper_id        bigint    not null comment '试卷ID',
    exam_index      int       not null default 1 comment '考试坐标',
    grade_score     double    not null default 0 comment '成绩',
    correct_count   int       not null default 0 comment '正确数',
    fault_count     int       not null default 0 comment '错误数',
    topic_count     int       not null default 0 comment '答题总数',
    exam_spend_time int       not null default 0 comment '花费时间',
    insert_time     timestamp not null comment '插入时间',
    update_time     timestamp not null default current_timestamp comment '更新时间',
    delete_time     datetime  not null comment '删除时间',
    index index_grade_use (user_id),
    index index_grade_exam_index (paper_id, exam_index)
) charset utf8;

create table tb_exam_record
(
    id            bigint primary key auto_increment comment '记录ID',
    paper_id      bigint    not null comment '试卷ID',
    user_id       char(10)  not null comment '用户ID',
    exam_index    int       not null default 0 comment '答题坐标',
    topic_id      bigint    not null comment '题目ID',
    question_id   bigint    not null comment '问题ID',
    user_answer   char(10)  not null default '' comment '用户答案',
    actual_answer char(10)  not null default '' comment '正确答案',
    record_score  double    not null default 0 comment '分数',
    insert_time   timestamp not null comment '插入时间',
    update_time   timestamp not null default current_timestamp comment '更新时间',
    delete_time   datetime  not null comment '删除时间',
    index index_record_user (user_id),
    index index_record_exam_index (paper_id, exam_index),
    index index_record_topic_question (topic_id, question_id)
) charset utf8;