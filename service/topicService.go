package service

import (
	"Panda/common/exception"
	"Panda/common/logs"
	"Panda/module/inter"
	"Panda/pojo/entity"
	"Panda/pojo/model"
	"Panda/proxy"
	"context"

	"github.com/jinzhu/gorm"
)

func init() {
	inter.RegisterService("TopicService", &topicService)
}

type TopicService struct {
	db              *gorm.DB
	topicProxy      *proxy.TopicProxy
	questionService *QuestionService
}

var topicService TopicService

func (s *TopicService) InitService(db *gorm.DB) {
	topicService = TopicService{
		db:              db,
		topicProxy:      &proxy.TopicProxy{},
		questionService: GetQuestionService(),
	}
}

func GetTopicService() *TopicService {
	return &topicService
}

func (s *TopicService) InsertTopic(ctx context.Context, topic model.Topic) (int64, error) {
	req := entity.Topic{
		ID:        topic.ID,
		TopicText: topic.TopicText,
		TopicType: topic.TopicType,
		TeamID:    topic.TeamID,
		CreatorID: topic.CreatorID,
	}

	s.db.Begin()

	topicID, err := s.topicProxy.InsertTopic(s.db, req)
	if err != nil {
		logs.Error(ctx, "")
		return 0, err
	}

	for _, question := range topic.Questions {
		_, err := s.questionService.InsertQuestion(ctx, question)
		if err != nil {
			logs.Error(ctx, "")
			return 0, err
		}
	}

	s.db.Commit()

	return topicID, nil
}

func (s *TopicService) UpdateTopic(ctx context.Context, topic model.Topic) (int64, error) {
	req := entity.Topic{
		ID:        topic.ID,
		TopicText: topic.TopicText,
		TopicType: topic.TopicType,
		TeamID:    topic.TeamID,
		CreatorID: topic.CreatorID,
	}

	row, err := s.topicProxy.UpdateTopicByCond(s.db, req)
	if err != nil {
		logs.Error(ctx, "")
		return 0, err
	}

	return row, nil
}

func (s *TopicService) DeleteTopic(ctx context.Context, topic model.Topic) (int64, error) {
	req := entity.Topic{
		ID:        topic.ID,
		TopicType: topic.TopicType,
		TeamID:    topic.TeamID,
		CreatorID: topic.CreatorID,
	}

	count, err := s.topicProxy.DeleteTopicByCond(s.db, req)
	if err != nil {
		logs.Error(ctx, "")
		return 0, err
	}

	return count, nil
}

func (s *TopicService) GetTopicByID(ctx context.Context, topic model.Topic) (model.Topic, error) {
	req := entity.Topic{
		ID: topic.ID,
	}

	res, err := s.topicProxy.SelectTopicByCond(s.db, req, nil)
	if err != nil {
		logs.Error(ctx, "")
		return model.Topic{}, err
	}

	if len(res) != 1 {
		logs.Error(ctx, "")
		return model.Topic{}, exception.DataNotExist
	}
	return res[0].ToModel(), nil
}

func (s *TopicService) GetTopicListPage(ctx context.Context, topic model.Topic, page *entity.Page) ([]model.Topic, int64, error) {
	req := entity.Topic{
		ID:        topic.ID,
		TopicText: topic.TopicText,
		TopicType: topic.TopicType,
		TeamID:    topic.TeamID,
		CreatorID: topic.CreatorID,
	}

	count, err := s.topicProxy.CountTopicByCond(s.db, req)
	if err != nil {
		logs.Error(ctx, "")
		return nil, 0, err
	}

	if count == 0 {
		logs.Warn(ctx, "")
		return nil, 0, exception.DataNotExist
	}

	list, err := s.topicProxy.SelectTopicByCond(s.db, req, page)
	if err != nil {
		logs.Error(ctx, "")
		return nil, 0, err
	}

	res := make([]model.Topic, len(list))
	for _, item := range list {
		res = append(res, item.ToModel())
	}
	return res, 0, nil
}
