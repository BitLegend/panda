package service

import (
	"Panda/common/constant"
	"Panda/common/exception"
	"Panda/common/logs"
	"Panda/common/util"
	"Panda/module/inter"
	"Panda/pojo/entity"
	"Panda/pojo/model"
	"Panda/proxy"
	"context"
	"time"

	"github.com/jinzhu/gorm"
)

func init() {
	inter.RegisterService("UserService", &userService)
}

var userService UserService

type UserService struct {
	db        *gorm.DB
	userProxy *proxy.UserProxy
	teamProxy *proxy.TeamProxy
}

func (s *UserService) InitService(db *gorm.DB) {
	userService = UserService{
		db:        db,
		userProxy: &proxy.UserProxy{},
	}
	return
}

func GetUserService() *UserService {
	return &userService
}

func (s *UserService) UserLogin(ctx context.Context, auth *model.User) (*model.User, error) {
	if auth == nil || auth.UserID == "" || auth.UserPassword == "" {
		logs.Error(ctx, "")
		return nil, exception.RequestParamsIllegal
	}

	// 获取数据
	query := entity.User{
		UserID: auth.UserID,
	}
	page := entity.Page{
		PageSize:  1,
		LineStart: 0,
	}

	users, err := s.userProxy.SelectUserByCond(s.db, query, &page)
	if err != nil {
		logs.Error(ctx, "")
		return nil, err
	}
	if len(users) == 0 {
		logs.Warn(ctx, "")
		return nil, exception.DataNotExist
	}
	if users[0].UserPassword != auth.UserPassword {
		logs.Warn(ctx, "")
		return nil, exception.AuthenticationFail
	}

	resp := users[0].ToModel()

	// generate login token
	resp.LoginToken, err = util.GetToken(resp, constant.TokenGenKey, 2*time.Hour)
	if err != nil {
		logs.Error(ctx, "")
		return nil, exception.TokenGenerateFail
	}

	return &resp, nil
}

func (s *UserService) UserRegister(ctx context.Context, query model.User) (*model.User, error) {
	// 查询用户是否存在
	user := entity.User{
		UserID: query.UserID,
	}
	old, err := s.userProxy.SelectUserByCond(s.db, user, nil)
	if err != nil {
		logs.Error(ctx, "")
		if err == gorm.ErrRecordNotFound {
			return nil, exception.DataNotExist
		}
		return nil, err
	}
	if old == nil {
		// 检查团队是否存在
		teamQuery := entity.Team{
			ID: query.UserTeamID,
		}
		count := int64(0)
		if count, err = s.teamProxy.CountTeamByCond(s.db, teamQuery); err != nil {
			logs.Error(ctx, "")
			return nil, err
		}
		if count == 0 {
			logs.Error(ctx, "")
			return nil, exception.TeamDataNotFound
		}

		// 检查学生的邮箱是否重复注册
		userQuery := entity.User{
			UserMail: query.UserMail,
		}
		if count, err = s.userProxy.CountUserByCond(s.db, userQuery); err != nil {
			logs.Error(ctx, "")
			return nil, err
		}
		if count > 0 {
			logs.Error(ctx, "")
			return nil, exception.MailAlreadyUsed
		}

		// 插入学生信息
		user = entity.User{
			UserID:       query.UserID,
			UserPassword: query.UserPassword,
			UserGender:   int64(query.UserGender.Code),
			UserName:     query.UserName,
			UserMail:     query.UserMail,
			UserTel:      query.UserTel,
			UserAddress:  query.UserAddress,
			UserTeamID:   query.UserTeamID,
			UserRole:     int64(query.UserRole.Code),
		}
		err := s.userProxy.InsertUser(s.db, user)
		if err != nil {
			logs.Error(ctx, "")
			return nil, err
		}
		resp := user.ToModel()
		return &resp, nil
	}
	return nil, exception.DataExist
}

func (s *UserService) GetUserByID(ctx context.Context, query model.User) (*model.User, error) {
	if query.UserID == "" {
		logs.Error(ctx, "")
		return nil, exception.RequestParamsIllegal
	}
	req := entity.User{
		UserID: query.UserID,
	}
	users, err := s.userProxy.SelectUserByCond(s.db, req, nil)
	if err != nil {
		logs.Error(ctx, "")
		if err == gorm.ErrRecordNotFound {
			return nil, exception.DataNotExist
		}
		return nil, err
	}
	if len(users) != 1 {
		logs.Error(ctx, "")
		return nil, exception.DataNotExist
	}
	resp := users[0].ToModel()
	return &resp, nil
}

func (s *UserService) DeleteUserByCond(ctx context.Context, query model.User) (int64, error) {
	req := entity.User{
		UserID:     query.UserID,
		UserTeamID: query.UserTeamID,
		UserRole:   int64(query.UserRole.Code),
	}

	row, err := s.userProxy.DeleteUserByCond(s.db, req)
	if err != nil {
		logs.Error(ctx, "")
		if err == gorm.ErrRecordNotFound {
			return 0, exception.DataNotExist
		}
		return 0, err
	}

	return row, nil
}

func (s *UserService) GetUserListByPage(ctx context.Context, query model.User, page *entity.Page) ([]model.User, int64, error) {
	req := entity.User{
		UserGender: int64(query.UserGender.Code),
		UserName:   query.UserName,
		UserMail:   query.UserMail,
		UserTel:    query.UserTel,
		UserTeamID: query.UserTeamID,
		UserRole:   int64(query.UserRole.Code),
	}

	count, err := s.userProxy.CountUserByCond(s.db, req)
	if err != nil {
		logs.Error(ctx, "")
		return nil, 0, err
	}

	if count > 0 {
		logs.Error(ctx, "")
		return nil, count, nil
	}

	rows, err := s.userProxy.SelectUserByCond(s.db, req, page)

	if err != nil {
		logs.Error(ctx, "")
		if err == gorm.ErrRecordNotFound {
			return nil, 0, exception.DataNotExist
		}
		return nil, 0, err
	}

	userList := make([]model.User, 0, len(rows))
	if len(rows) > 0 {
		for _, user := range rows {
			userList = append(userList, user.ToModel())
		}
	}

	return userList, count, nil
}

func (s *UserService) UpdateUserByID(ctx context.Context, user model.User) (int64, error) {
	req := entity.User{
		UserID:      user.UserID,
		UserGender:  int64(user.UserGender.Code),
		UserName:    user.UserName,
		UserMail:    user.UserMail,
		UserTel:     user.UserTel,
		UserAddress: user.UserAddress,
		UserTeamID:  user.UserTeamID,
		UserRole:    int64(user.UserRole.Code),
	}

	row, err := s.userProxy.UpdateUserByCond(s.db, req)
	if err != nil {
		logs.Error(ctx, "")
		return 0, err
	}

	return row, nil
}

func (s *UserService) UpdateUserPwd(ctx context.Context, user model.User, oldPassword string) (string, error) {
	req := entity.User{
		UserID:       user.UserID,
		UserPassword: user.UserPassword,
	}

	old, err := s.GetUserByID(ctx, user)
	if err != nil {
		logs.Error(ctx, "")
		if err == gorm.ErrRecordNotFound {
			return "", exception.DataNotExist
		}
		return "", err
	}

	if old.UserPassword != oldPassword {
		logs.Error(ctx, "")
		return "", exception.RequestParamsIllegal
	}

	err = s.userProxy.UpdateUserPwd(s.db, req)
	if err != nil {
		logs.Error(ctx, "")
		return "", err
	}

	return req.UserID, nil
}
