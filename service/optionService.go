package service

import (
	"Panda/common/exception"
	"Panda/module/inter"
	"Panda/pojo/entity"
	"Panda/pojo/model"
	"Panda/proxy"
	"context"

	"github.com/jinzhu/gorm"
)

func init() {
	inter.RegisterService("OptionService", &optionService)
}

type OptionService struct {
	db            *gorm.DB
	optionProxy   *proxy.OptionProxy
	questionProxy *proxy.QuestionProxy
}

var optionService OptionService

func (o *OptionService) InitService(db *gorm.DB) {
	optionService = OptionService{
		db:            db,
		optionProxy:   &proxy.OptionProxy{},
		questionProxy: &proxy.QuestionProxy{},
	}
	return
}

func GetOptionService() *OptionService {
	return &optionService
}

func (o *OptionService) InsertOption(ctx context.Context, option model.Option) (int64, error) {
	req := entity.Option{
		ID:         option.ID,
		OptionText: option.OptionText,
		OptionKey:  option.OptionKey,
		QuestionID: option.QuestionID,
		CreatorID:  option.CreatorID,
	}

	query := entity.Question{
		ID: option.QuestionID,
	}

	count, err := o.questionProxy.CountQuestionByCond(o.db, query)
	if err != nil {
		return 0, err
	}

	if count == 0 {
		return 0, exception.DataNotExist
	}

	id, err := o.optionProxy.InsertOption(o.db, req)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (o *OptionService) UpdateOption(ctx context.Context, option model.Option) (int64, error) {
	req := entity.Option{
		ID:         option.ID,
		OptionText: option.OptionText,
		OptionKey:  option.OptionKey,
		QuestionID: option.QuestionID,
		CreatorID:  option.CreatorID,
	}

	id, err := o.optionProxy.UpdateOptionByCond(o.db, req)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (o *OptionService) DeleteOption(ctx context.Context, option model.Option) (int64, error) {
	req := entity.Option{
		ID:         option.ID,
		QuestionID: option.QuestionID,
		CreatorID:  option.CreatorID,
	}

	row, err := o.optionProxy.DeleteOptionByCond(o.db, req)
	if err != nil {
		return 0, err
	}

	return row, nil
}

func (o *OptionService) GetOption(ctx context.Context, option model.Option) (model.Option, error) {
	req := entity.Option{
		ID:         option.ID,
		QuestionID: option.QuestionID,
		CreatorID:  option.CreatorID,
	}

	list, err := o.optionProxy.SelectOptionByCond(o.db, req, nil)
	if err != nil {
		return model.Option{}, err
	}

	if len(list) != 1 {
		return model.Option{}, exception.DataNotExist
	}

	return list[0].ToModel(), nil
}

func (o *OptionService) GetOptionByPage(ctx context.Context, option model.Option, page *entity.Page) ([]model.Option, int64, error) {
	req := entity.Option{
		ID:         option.ID,
		QuestionID: option.QuestionID,
		CreatorID:  option.CreatorID,
	}

	count, err := o.optionProxy.CountOptionByCond(o.db, req)
	if err != nil {
		return nil, 0, err
	}

	if count == 0 {
		return nil, 0, exception.DataNotExist
	}

	list, err := o.optionProxy.SelectOptionByCond(o.db, req, page)
	if err != nil {
		return nil, 0, err
	}

	res := make([]model.Option, len(list))

	for _, item := range list {
		res = append(res, item.ToModel())
	}

	return res, 0, nil
}
