package service

import (
	"Panda/common/exception"
	"Panda/module/inter"
	"Panda/pojo/entity"
	"Panda/pojo/model"
	"Panda/proxy"
	"context"

	"github.com/jinzhu/gorm"
)

func init() {
	inter.RegisterService("TeamService", &teamService)
}

type TeamService struct {
	db        *gorm.DB
	teamProxy *proxy.TeamProxy
}

var teamService TeamService

func (s *TeamService) InitService(db *gorm.DB) {
	teamService = TeamService{
		db:        db,
		teamProxy: &proxy.TeamProxy{},
	}
	return
}

func GetTeamService() *TeamService {
	return &teamService
}

func (s *TeamService) InsertTeam(ctx context.Context, team model.Team) (int64, error) {
	req := entity.Team{
		ID:              team.ID,
		TeamName:        team.TeamName,
		TeamIntroduce:   team.TeamIntroduce,
		TeamMotto:       team.TeamMotto,
		TeamMemberCount: team.TeamMemberCount,
		TeamMail:        team.TeamMail,
		TeamTel:         team.TeamTel,
		TeamAddress:     team.TeamAddress,
		CreatorID:       team.CreatorID,
	}

	id, err := s.teamProxy.InsertTeam(s.db, req)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (s *TeamService) UpdateTeam(ctx context.Context, team model.Team) (int64, error) {
	req := entity.Team{
		ID:              team.ID,
		TeamName:        team.TeamName,
		TeamIntroduce:   team.TeamIntroduce,
		TeamMotto:       team.TeamMotto,
		TeamMemberCount: team.TeamMemberCount,
		TeamMail:        team.TeamMail,
		TeamTel:         team.TeamTel,
		TeamAddress:     team.TeamAddress,
		CreatorID:       team.CreatorID,
	}

	row, err := s.teamProxy.UpdateTeamByCond(s.db, req)
	if err != nil {
		return 0, err
	}
	return row, nil
}

func (s *TeamService) DeleteTeam(ctx context.Context, team model.Team) (int64, error) {
	req := entity.Team{
		ID:        team.ID,
		CreatorID: team.CreatorID,
	}
	count, err := s.teamProxy.DeleteTeamByCond(s.db, req)
	if err != nil {
		return 0, err
	}
	return count, nil
}

func (s *TeamService) GetTeamByID(ctx context.Context, team model.Team) (model.Team, error) {
	req := entity.Team{
		ID: team.ID,
	}

	res, err := s.teamProxy.SelectTeamByCond(s.db, req, nil)
	if err != nil {
		return model.Team{}, err
	}

	if len(res) != 1 {
		return model.Team{}, exception.DataNotExist
	}
	return res[0].ToModel(), nil
}

func (s *TeamService) GetTeamByCond(ctx context.Context, team model.Team, page *entity.Page) ([]model.Team, int64, error) {
	req := entity.Team{
		ID:        team.ID,
		TeamName:  team.TeamName,
		CreatorID: team.CreatorID,
	}

	count, err := s.teamProxy.CountTeamByCond(s.db, req)
	if err != nil {
		return nil, 0, err
	}

	if count == 0 {
		return make([]model.Team, 0), 0, nil
	}

	res, err := s.teamProxy.SelectTeamByCond(s.db, req, page)
	if err != nil {
		return nil, 0, err
	}

	list := make([]model.Team, len(res))
	for _, item := range res {
		list = append(list, item.ToModel())
	}
	return list, count, nil
}
