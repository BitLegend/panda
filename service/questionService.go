package service

import (
	"Panda/common/exception"
	"Panda/common/logs"
	"Panda/module/inter"
	"Panda/pojo/entity"
	"Panda/pojo/model"
	"Panda/proxy"
	"context"

	"github.com/jinzhu/gorm"
)

func init() {
	inter.RegisterService("QuestionService", &questionService)
}

type QuestionService struct {
	db            *gorm.DB
	questionProxy *proxy.QuestionProxy
	optionService *OptionService
}

var questionService QuestionService

func (q *QuestionService) InitService(db *gorm.DB) {
	questionService = QuestionService{
		db:            db,
		questionProxy: &proxy.QuestionProxy{},
		optionService: GetOptionService(),
	}
	return
}

func GetQuestionService() *QuestionService {
	return &questionService
}

func (q *QuestionService) InsertQuestion(ctx context.Context, question model.Question) (int64, error) {
	req := entity.Question{
		ID:             question.ID,
		QuestionText:   question.QuestionText,
		QuestionType:   question.QuestionType,
		QuestionAnswer: question.QuestionAnswer,
		QuestionScore:  question.QuestionScore,
		TopicID:        question.TopicID,
	}

	id, err := q.questionProxy.InsertQuestion(q.db, req)
	if err != nil {
		return 0, err
	}

	for _, option := range question.Options {
		_, err := q.optionService.InsertOption(ctx, option)
		if err != nil {
			logs.Error(ctx, "")
			return 0, err
		}
	}
	return id, nil
}

func (q *QuestionService) UpdateQuestion(ctx context.Context, question model.Question) (int64, error) {
	req := entity.Question{
		ID:             question.ID,
		QuestionText:   question.QuestionText,
		QuestionType:   question.QuestionType,
		QuestionAnswer: question.QuestionAnswer,
		QuestionScore:  question.QuestionScore,
		TopicID:        question.TopicID,
		CreatorID:      question.CreatorID,
	}
	row, err := q.questionProxy.UpdateQuestionCond(q.db, req)
	if err != nil {
		return 0, err
	}
	return row, nil
}

func (q *QuestionService) DeleteQuestion(ctx context.Context, question model.Question) (int64, error) {
	req := entity.Question{
		ID:           question.ID,
		QuestionType: question.QuestionType,
		TopicID:      question.TopicID,
		CreatorID:    question.CreatorID,
	}

	count, err := q.questionProxy.DeleteQuestionByCond(q.db, req)
	if err != nil {
		return 0, nil
	}
	return count, nil
}

func (q *QuestionService) GetQuestionByID(ctx context.Context, question model.Question) (model.Question, error) {
	req := entity.Question{
		ID: question.ID,
	}

	res, err := q.questionProxy.SelectQuestionByCond(q.db, req, nil)
	if err != nil {
		return model.Question{}, err
	}
	if len(res) != 1 {
		return model.Question{}, exception.DataNotExist
	}
	return res[0].ToModel(), nil
}

func (q *QuestionService) GetQuestionByPage(ctx context.Context, question model.Question, page *entity.Page) ([]model.Question, int64, error) {
	return nil, 0, nil
}
