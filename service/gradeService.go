package service

import (
	"Panda/common/exception"
	"Panda/module/inter"
	"Panda/pojo/entity"
	"Panda/pojo/model"
	"Panda/proxy"
	"context"

	"github.com/jinzhu/gorm"
)

func init() {
	inter.RegisterService("GradeService", &gradeService)
}

type GradeService struct {
	db         *gorm.DB
	gradeProxy *proxy.GradeProxy
}

var gradeService GradeService

func (g *GradeService) InitService(db *gorm.DB) {
	gradeService = GradeService{
		db:         db,
		gradeProxy: &proxy.GradeProxy{},
	}
	return
}

func GetGradeService() *GradeService {
	return &gradeService
}

func (g *GradeService) InsertGrade(ctx context.Context, grade model.Grade) (int64, error) {
	req := entity.Grade{
		UserID:        grade.UserID,
		PaperID:       grade.PaperID,
		ExamIndex:     grade.ExamIndex,
		GradeScore:    grade.GradeScore,
		CorrectCount:  grade.CorrectCount,
		FaultCount:    grade.FaultCount,
		TopicCount:    grade.TopicCount,
		ExamSpendTime: grade.ExamSpendTime,
	}

	id, err := g.gradeProxy.InsertGrade(g.db, req)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (g *GradeService) UpdateGrade(ctx context.Context, grade model.Grade) (int64, error) {
	req := entity.Grade{
		UserID:        grade.UserID,
		PaperID:       grade.PaperID,
		ExamIndex:     grade.ExamIndex,
		GradeScore:    grade.GradeScore,
		CorrectCount:  grade.CorrectCount,
		FaultCount:    grade.FaultCount,
		TopicCount:    grade.TopicCount,
		ExamSpendTime: grade.ExamSpendTime,
	}

	row, err := g.gradeProxy.UpdateGradeByCond(g.db, req)
	if err != nil {
		return 0, err
	}
	return row, nil
}

func (g *GradeService) DeleteGrade(ctx context.Context, grade model.Grade) (int64, error) {
	req := entity.Grade{
		ID:        grade.ID,
		UserID:    grade.UserID,
		PaperID:   grade.PaperID,
		ExamIndex: grade.ExamIndex,
	}

	row, err := g.gradeProxy.DeleteGradeByCond(g.db, req)
	if err != nil {
		return 0, err
	}
	return row, nil
}

func (g *GradeService) GetGradeByID(ctx context.Context, grade model.Grade) (model.Grade, error) {
	req := entity.Grade{
		ID:        0,
		UserID:    "",
		PaperID:   0,
		ExamIndex: 0,
	}

	list, err := g.gradeProxy.SelectGradeByCond(g.db, req, nil)
	if err != nil {
		return model.Grade{}, err
	}

	if len(list) != 1 {
		return model.Grade{}, exception.DataNotExist
	}

	return list[0].ToModel(), nil
}

func (g *GradeService) GetGradeByPage(ctx context.Context, grade model.Grade, page *entity.Page) ([]model.Grade, int64, error) {
	req := entity.Grade{
		ID:        0,
		UserID:    "",
		PaperID:   0,
		ExamIndex: 0,
	}

	count, err := g.gradeProxy.CountGradeByCond(g.db, req)
	if err != nil {
		return nil, 0, err
	}

	if count == 0 {
		return nil, 0, exception.DataNotExist
	}

	list, err := g.gradeProxy.SelectGradeByCond(g.db, req, page)
	if err != nil {
		return nil, 0, err
	}

	res := make([]model.Grade, len(list))
	for _, item := range list {
		res = append(res, item.ToModel())
	}

	return res, count, nil
}
