package service

import (
	"Panda/common/exception"
	"Panda/module/inter"
	"Panda/pojo/entity"
	"Panda/pojo/model"
	"Panda/proxy"
	"context"

	"github.com/jinzhu/gorm"
)

func init() {
	inter.RegisterService("ExamRecordService", &examRecordService)
}

type ExamRecordService struct {
	db              *gorm.DB
	examRecordProxy *proxy.ExamRecordProxy
}

var examRecordService ExamRecordService

func (e *ExamRecordService) InitService(db *gorm.DB) {
	examRecordService = ExamRecordService{
		db:              db,
		examRecordProxy: &proxy.ExamRecordProxy{},
	}
	return
}

func GetExamRecordService() *ExamRecordService {
	return &examRecordService
}

func (e *ExamRecordService) InsertExamRecord(ctx context.Context, record model.ExamRecord) (int64, error) {
	req := entity.ExamRecord{
		PaperID:      record.PaperID,
		UserID:       record.UserID,
		ExamIndex:    record.ExamIndex,
		TopicID:      record.TopicID,
		QuestionID:   record.QuestionID,
		UserAnswer:   record.UserAnswer,
		ActualAnswer: record.ActualAnswer,
		RecordScore:  record.RecordScore,
	}

	id, err := e.examRecordProxy.InsertExamRecord(e.db, req)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (e *ExamRecordService) UpdateExamRecord(ctx context.Context, record model.ExamRecord) (int64, error) {
	req := entity.ExamRecord{
		ID:           record.ID,
		PaperID:      record.PaperID,
		UserID:       record.UserID,
		ExamIndex:    record.ExamIndex,
		TopicID:      record.TopicID,
		QuestionID:   record.QuestionID,
		UserAnswer:   record.UserAnswer,
		ActualAnswer: record.ActualAnswer,
		RecordScore:  record.RecordScore,
	}

	row, err := e.examRecordProxy.UpdateExamRecordByCond(e.db, req)
	if err != nil {
		return 0, err
	}

	return row, nil
}

func (e *ExamRecordService) DeleteExamRecord(ctx context.Context, record model.ExamRecord) (int64, error) {
	req := entity.ExamRecord{
		ID:        record.ID,
		PaperID:   record.PaperID,
		UserID:    record.UserID,
		ExamIndex: record.ExamIndex,
	}

	row, err := e.examRecordProxy.DeleteExamRecordByCond(e.db, req)
	if err != nil {
		return 0, err
	}
	return row, nil
}

func (e *ExamRecordService) GetExamRecord(ctx context.Context, record model.ExamRecord) (model.ExamRecord, error) {
	req := entity.ExamRecord{
		ID:        record.ID,
		PaperID:   record.PaperID,
		UserID:    record.UserID,
		ExamIndex: record.ExamIndex,
	}

	list, err := e.examRecordProxy.SelectExamRecordByCond(e.db, req, nil)
	if err != nil {
		return model.ExamRecord{}, err
	}

	if len(list) != 1 {
		return model.ExamRecord{}, exception.DataNotExist
	}

	return list[0].ToModel(), nil
}

func (e *ExamRecordService) GetExamRecordByPage(ctx context.Context, record model.ExamRecord, page *entity.Page) ([]model.ExamRecord, int64, error) {
	req := entity.ExamRecord{
		ID:        record.ID,
		PaperID:   record.PaperID,
		UserID:    record.UserID,
		ExamIndex: record.ExamIndex,
	}

	count, err := e.examRecordProxy.CountExamRecordByCond(e.db, req)
	if err != nil {
		return nil, 0, err
	}

	if count == 0 {
		return nil, 0, err
	}

	list, err := e.examRecordProxy.SelectExamRecordByCond(e.db, req, page)
	if err != nil {
		return nil, 0, err
	}

	res := make([]model.ExamRecord, 0, len(list))
	for _, item := range list {
		res = append(res, item.ToModel())
	}
	return res, count, nil
}
