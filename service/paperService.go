package service

import (
	"Panda/common/exception"
	"Panda/module/inter"
	"Panda/pojo/entity"
	"Panda/pojo/model"
	"Panda/proxy"
	"context"

	"github.com/jinzhu/gorm"
)

func init() {
	inter.RegisterService("PaperService", &paperService)
}

type PaperService struct {
	db         *gorm.DB
	paperProxy *proxy.PaperProxy
}

var paperService PaperService

func (p *PaperService) InitService(db *gorm.DB) {
	paperService = PaperService{
		db:         db,
		paperProxy: &proxy.PaperProxy{},
	}
	return
}

func GetPaperService() *PaperService {
	return &paperService
}

func (p *PaperService) InsertPaper(ctx context.Context, paper model.Paper) (int64, error) {
	req := entity.Paper{
		ID:             paper.ID,
		PaperName:      paper.PaperName,
		PaperTeamID:    paper.PaperTeamID,
		PaperIntroduce: paper.PaperIntroduce,
		PaperStartTime: paper.PaperStartTime,
		PaperEndTime:   paper.PaperEndTime,
		ValidTimeCount: paper.ValidTimeCount,
		MaxJoinCount:   paper.MaxJoinCount,
		PaperPassScore: paper.PaperPassScore,
		PaperPassCount: paper.PaperPassCount,
		PaperJoinCount: paper.PaperJoinCount,
		CreatorID:      paper.CreatorID,
	}

	id, err := p.paperProxy.InsertPaper(p.db, req)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (p *PaperService) UpdatePaper(ctx context.Context, paper model.Paper) (int64, error) {
	req := entity.Paper{
		ID:             paper.ID,
		PaperName:      paper.PaperName,
		PaperTeamID:    paper.PaperTeamID,
		PaperIntroduce: paper.PaperIntroduce,
		PaperStartTime: paper.PaperStartTime,
		PaperEndTime:   paper.PaperEndTime,
		ValidTimeCount: paper.ValidTimeCount,
		MaxJoinCount:   paper.MaxJoinCount,
		PaperPassScore: paper.PaperPassScore,
		PaperPassCount: paper.PaperPassCount,
		PaperJoinCount: paper.PaperJoinCount,
	}

	row, err := p.paperProxy.UpdatePaperByCond(p.db, req)
	if err != nil {
		return 0, err
	}

	return row, nil
}

func (p *PaperService) DeletePaper(ctx context.Context, paper model.Paper) (int64, error) {
	req := entity.Paper{
		ID:          paper.ID,
		PaperTeamID: paper.PaperTeamID,
		CreatorID:   paper.CreatorID,
	}

	row, err := p.paperProxy.DeletePaperByCond(p.db, req)
	if err != nil {
		return 0, err
	}
	return row, nil
}

func (p *PaperService) GetPaperByID(ctx context.Context, paper model.Paper) (model.Paper, error) {
	req := entity.Paper{
		ID: paper.ID,
	}
	res, err := p.paperProxy.SelectPaperByCond(p.db, req, nil)
	if err != nil {
		return model.Paper{}, err
	}
	if len(res) != 1 {
		return model.Paper{}, exception.DataNotExist
	}
	return res[0].ToModel(), nil
}

func (p *PaperService) GetPaperByPage(ctx context.Context, paper model.Paper, page *entity.Page) ([]model.Paper, int64, error) {
	req := entity.Paper{
		ID:          paper.ID,
		PaperName:   paper.PaperName,
		PaperTeamID: paper.PaperTeamID,
		CreatorID:   paper.CreatorID,
	}

	count, err := p.paperProxy.CountPaperByCond(p.db, req)
	if err != nil {
		return nil, 0, err
	}
	if count == 0 {
		return nil, 0, exception.DataNotExist
	}

	res, err := p.paperProxy.SelectPaperByCond(p.db, req, page)
	if err != nil {
		return nil, 0, err
	}

	list := make([]model.Paper, len(res))

	for _, r := range res {
		list = append(list, r.ToModel())
	}

	return list, count, nil
}
