package main

import (
	"Panda/common/config"
	"Panda/common/constant"
	"Panda/common/exception"
	"Panda/common/logs"
	"Panda/common/util"
	"Panda/handlers"
	"Panda/pojo/model"
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
)

// SessionCheck 登录信息校验
func SessionCheck(ctx *gin.Context) {
	token, err := ctx.Cookie(constant.HttpTokenHeaderKey)
	if err != nil {
		logs.Error(ctx, "")
		return
	}
	if token != "" {
		userInfoStr, err := util.DecodeToken(token, constant.TokenGenKey)
		if err != nil || userInfoStr == "" {
			logs.Error(ctx, "")
			return
		}

		var userInfo model.User
		if err = json.Unmarshal([]byte(userInfoStr), &userInfo); err != nil {
			logs.Error(ctx, "")
			return
		}

		//log id
		logId := logs.GenLogID()
		ctx.Keys[constant.LogKey] = logId
		ctx.Next()
	}

	ctx.JSON(http.StatusOK, &config.Response{
		Code:    exception.AuthenticationFail.(*exception.Exception).Code,
		Message: exception.AuthenticationFail.(*exception.Exception).Explain,
	})
}

//InitContextVia do context init params
func InitContextVia(ctx *gin.Context) {
	// init keys map
	if ctx.Keys == nil {
		ctx.Keys = make(map[string]interface{})
	}
	ctx.Next()
}

// InitRouter 全局路由初始化
func InitRouter(router *gin.Engine) {

	//root router
	root := router.Group("/panda/api", InitContextVia)

	//open router
	public := root.Group("/public")
	//root check router
	private := root.Group("/private", SessionCheck)

	initPub(public)
	initUser(private)
	initTopic(private)
	initTeam(private)
	initQuestion(private)
	initPaper(private)
	initOption(private)
	initGrade(private)
	initExamRecord(private)
}

// initPub 开放路由组
func initPub(root *gin.RouterGroup) {
	r := root.Group("/v1")
	r.POST("/login", HandlerFunc(handlers.UserLogin))
	r.POST("/register", HandlerFunc(handlers.UserRegister))
}

// initUser 用户路由组
func initUser(root *gin.RouterGroup) {
	r := root.Group("/user/v1")
	r.POST("/insert", HandlerFunc(handlers.InsertUser))
	r.POST("/updatePwd", HandlerFunc(handlers.UpdateUserPwd))
	r.POST("/update", HandlerFunc(handlers.UpdateUser))
	r.GET("/query", HandlerFunc(handlers.GetUser))
	r.GET("/list", HandlerFunc(handlers.GetUserListPage))
}

// initTopic 题目路由组
func initTopic(root *gin.RouterGroup) {
	r := root.Group("/topic/v1")
	r.POST("/insert", HandlerFunc(handlers.InsertTopic))
	r.POST("/update", HandlerFunc(handlers.UpdateTopic))
	r.POST("/delete", HandlerFunc(handlers.DeleteTopic))
	r.GET("/topic", HandlerFunc(handlers.GetTopicByID))
	r.GET("/list", HandlerFunc(handlers.GetTopicByPage))
}

// initTeam 团队信息路由组
func initTeam(root *gin.RouterGroup) {
	r := root.Group("/team/v1")
	r.POST("/insert", HandlerFunc(handlers.InsertTeam))
	r.POST("/update", HandlerFunc(handlers.UpdateTeam))
	r.POST("/delete", HandlerFunc(handlers.DeleteTeam))
	r.GET("/team", HandlerFunc(handlers.GetTeam))
	r.GET("/list", HandlerFunc(handlers.GetTeamByPage))
}

// initQuestion 问题信息路由组
func initQuestion(root *gin.RouterGroup) {
	r := root.Group("/question/v1")
	r.POST("/insert", HandlerFunc(handlers.InsertQuestion))
	r.POST("/update", HandlerFunc(handlers.UpdateQuestion))
	r.POST("/delete", HandlerFunc(handlers.DeleteQuestion))
	r.GET("/question", HandlerFunc(handlers.GetQuestion))
	r.GET("/list", HandlerFunc(handlers.GetQuestionByPage))
}

// initPaper 试卷路由组
func initPaper(root *gin.RouterGroup) {
	r := root.Group("/paper/v1")
	r.POST("/insert", HandlerFunc(handlers.InsertPaper))
	r.POST("/update", HandlerFunc(handlers.UpdatePaper))
	r.POST("/delete", HandlerFunc(handlers.DeletePaper))
	r.GET("/paper", HandlerFunc(handlers.GetPaper))
	r.GET("/list", HandlerFunc(handlers.GetPaperByPage))
}

// initOption 选项路由组
func initOption(root *gin.RouterGroup) {
	r := root.Group("/option/v1")
	r.POST("/insert", HandlerFunc(handlers.InsertOption))
	r.POST("/update", HandlerFunc(handlers.UpdateOption))
	r.POST("/delete", HandlerFunc(handlers.DeleteOption))
	r.GET("/option", HandlerFunc(handlers.GetOption))
	r.GET("/list", HandlerFunc(handlers.GetOptionByPage))
}

// initGrade 成绩路由组
func initGrade(root *gin.RouterGroup) {
	r := root.Group("/grade/v1")
	r.POST("/insert", HandlerFunc(handlers.InsertGrade))
	r.POST("/update", HandlerFunc(handlers.UpdateGrade))
	r.POST("/delete", HandlerFunc(handlers.DeleteGrade))
	r.GET("/grade", HandlerFunc(handlers.GetGrade))
	r.GET("/list", HandlerFunc(handlers.GetGradeByPage))
}

// initExamRecord 答题记录路由组
func initExamRecord(root *gin.RouterGroup) {
	r := root.Group("/exam_record/v1")
	r.POST("/insert", HandlerFunc(handlers.InsertExamRecord))
	r.POST("/update", HandlerFunc(handlers.UpdateExamRecord))
	r.POST("/delete", HandlerFunc(handlers.DeleteExamRecord))
	r.GET("/exam_record", HandlerFunc(handlers.GetExamRecord))
	r.GET("/list", HandlerFunc(handlers.GetExamRecordByPage))
}
