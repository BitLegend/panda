package handlers

import (
	"Panda/common/constant"
	"Panda/common/engine"
	"Panda/pojo/model"
	"Panda/pojo/request"
	"Panda/pojo/response"
	"Panda/service"
)

func InsertPaper(ctx *engine.Context) (interface{}, error) {
	var req request.PaperInsertRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.Paper{
		ID:             req.PaperID,
		PaperName:      req.PaperName,
		PaperIntroduce: req.PaperIntroduce,
		PaperTeamID:    req.PaperTeamID,
		PaperStartTime: req.PaperStartTime,
		PaperEndTime:   req.PaperEndTime,
		ValidTimeCount: req.ValidTimeCount,
		MaxJoinCount:   req.MaxJoinCount,
		PaperPassScore: req.PaperPassScore,
	}

	id, err := service.GetPaperService().InsertPaper(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func DeletePaper(ctx *engine.Context) (interface{}, error) {
	var req request.PaperQueryRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, nil
	}

	query := model.Paper{
		ID:        req.PaperID,
		PaperName: req.PaperName,
		CreatorID: ctx.Session[constant.CtxCreatorID],
	}

	id, err := service.GetPaperService().DeletePaper(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func UpdatePaper(ctx *engine.Context) (interface{}, error) {
	var req request.PaperUpdateRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	query := model.Paper{
		ID:             req.PaperID,
		PaperName:      req.PaperName,
		PaperIntroduce: req.PaperIntroduce,
		PaperStartTime: req.PaperStartTime,
		PaperEndTime:   req.PaperEndTime,
		ValidTimeCount: req.ValidTimeCount,
		MaxJoinCount:   req.MaxJoinCount,
		PaperPassScore: req.PaperPassScore,
		CreatorID:      ctx.Session[constant.CtxCreatorID],
	}

	id, err := service.GetPaperService().UpdatePaper(ctx, query)
	if err != nil {
		return nil, err
	}
	return id, nil
}

func GetPaper(ctx *engine.Context) (interface{}, error) {
	var req request.PaperQueryRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	query := model.Paper{
		ID:        req.PaperID,
		PaperName: req.PaperName,
	}

	paper, err := service.GetPaperService().GetPaperByID(ctx, query)
	if err != nil {
		return nil, err
	}
	return paper, nil
}

func GetPaperByPage(ctx *engine.Context) (interface{}, error) {
	var req request.PaperQueryRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	if err := ctx.BindQuery(&req.Page); err != nil {
		return nil, err
	}

	query := model.Paper{
		ID:          0,
		PaperName:   "",
		PaperTeamID: 0,
		CreatorID:   "",
	}

	list, count, err := service.GetPaperService().GetPaperByPage(ctx, query, nil)
	if err != nil {
		return nil, err
	}
	return response.ToPageResponse(list, req.PageIndex, req.PageSize, count), nil
}
