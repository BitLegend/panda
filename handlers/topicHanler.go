package handlers

import (
	"Panda/common/engine"
	"Panda/common/logs"
	"Panda/pojo/model"
	"Panda/pojo/request"
	"Panda/pojo/response"
	"Panda/service"
	"encoding/json"
)

func GetTopicByID(ctx *engine.Context) (interface{}, error) {
	req := request.TopicQueryReq{}
	if err := ctx.BindQuery(req); err != nil {
		return nil, err
	}

	query := model.Topic{
		ID: req.TopicID,
	}
	topic, err := service.GetTopicService().GetTopicByID(ctx, query)
	if err != nil {
		return nil, err
	}

	return topic.ToResponse(), nil
}

func UpdateTopic(ctx *engine.Context) (interface{}, error) {
	req := request.TopicUpdateReq{}
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.Topic{
		ID:        req.TopicID,
		TopicType: req.TopicType,
		TopicText: req.TopicText,
	}
	id, err := service.GetTopicService().UpdateTopic(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func DeleteTopic(ctx *engine.Context) (interface{}, error) {
	req := request.TopicDelReq{}
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	query := model.Topic{
		ID: req.TopicID,
	}

	id, err := service.GetTopicService().DeleteTopic(ctx, query)
	if err != nil {
		return nil, err
	}
	return id, nil
}

func InsertTopic(ctx *engine.Context) (interface{}, error) {
	req := request.TopicRequest{}
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	var questions []model.Question
	err := json.Unmarshal([]byte(req.QuestionStr), &questions)
	if err != nil {
		logs.Error(ctx, "")
		return nil, err
	}

	query := model.Topic{
		ID:        req.TopicID,
		TopicText: req.TopicText,
		TopicType: req.TopicType,
		TeamID:    req.TeamID,
		Questions: questions,
	}

	id, err := service.GetTopicService().InsertTopic(ctx, query)
	if err != nil {
		return nil, err
	}
	return id, nil
}

func GetTopicByPage(ctx *engine.Context) (interface{}, error) {
	req := request.TopicQueryReq{}
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	if req.PageSize > 0 {
		if req.PageIndex < 1 {
			req.PageIndex = 1
		}
		req.LineStart = (req.PageIndex - 1) * req.PageSize
	}

	query := model.Topic{
		ID:        req.TopicID,
		TopicText: req.TopicText,
		TeamID:    req.TeamID,
		TopicType: req.TopicType,
		CreatorID: req.CreatorID,
	}

	list, count, err := service.GetTopicService().GetTopicListPage(ctx, query, &req.Page)
	if err != nil {
		return nil, err
	}
	return response.ToPageResponse(list, req.PageIndex, req.PageSize, count), nil
}
