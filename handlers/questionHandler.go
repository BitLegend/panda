package handlers

import (
	"Panda/common/constant"
	"Panda/common/engine"
	"Panda/pojo/model"
	"Panda/pojo/request"
	"Panda/pojo/response"
	"Panda/service"
)

func InsertQuestion(ctx *engine.Context) (interface{}, error) {
	var req request.QuestionInsertRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}
	query := model.Question{
		QuestionText:   req.QuestionText,
		QuestionType:   req.QuestionType,
		QuestionAnswer: req.QuestionAnswer,
		QuestionScore:  req.QuestionScore,
		TopicID:        req.TopicID,
		CreatorID:      ctx.Session[constant.CtxCreatorID],
	}

	id, err := service.GetQuestionService().InsertQuestion(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func UpdateQuestion(ctx *engine.Context) (interface{}, error) {
	var req request.QuestionUpdateRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.Question{
		QuestionText:   req.QuestionText,
		QuestionType:   req.QuestionType,
		QuestionAnswer: req.QuestionAnswer,
		QuestionScore:  req.QuestionScore,
		CreatorID:      ctx.Session[constant.CtxCreatorID],
	}
	id, err := service.GetQuestionService().UpdateQuestion(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func DeleteQuestion(ctx *engine.Context) (interface{}, error) {
	var req request.QuestionUpdateRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.Question{
		ID:        req.QuestionID,
		CreatorID: ctx.Session[constant.CtxCreatorID],
	}

	id, err := service.GetQuestionService().DeleteQuestion(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func GetQuestion(ctx *engine.Context) (interface{}, error) {
	var req request.QuestionQueryRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	query := model.Question{
		ID:        req.QuestionID,
		TopicID:   req.TopicID,
		CreatorID: req.CreatorID,
	}

	topic, err := service.GetQuestionService().GetQuestionByID(ctx, query)
	if err != nil {
		return nil, err
	}
	return topic, nil
}

func GetQuestionByPage(ctx *engine.Context) (interface{}, error) {
	var req request.QuestionQueryRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	if err := ctx.BindQuery(&req.Page); err != nil {
		return nil, err
	}

	if req.PageSize > 0 {
		if req.PageIndex <= 0 {
			req.PageIndex = 1
		}
		req.LineStart = (req.PageIndex - 1) * req.PageSize
	}

	query := model.Question{
		ID:           req.QuestionID,
		QuestionText: req.QuestionText,
		QuestionType: req.QuestionType,
		TopicID:      req.TopicID,
		CreatorID:    req.CreatorID,
	}

	topics, count, err := service.GetQuestionService().GetQuestionByPage(ctx, query, &req.Page)
	if err != nil {
		return nil, err
	}

	return response.ToPageResponse(topics, req.PageIndex, req.PageSize, count), nil
}
