package handlers

import (
	"Panda/common/engine"
	"Panda/pojo/model"
	"Panda/pojo/request"
	"Panda/pojo/response"
	"Panda/service"
)

func InsertGrade(ctx *engine.Context) (interface{}, error) {
	var req request.GradeInsertRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.Grade{
		ID:            req.GradeID,
		UserID:        req.UserID,
		PaperID:       req.PaperID,
		ExamIndex:     req.ExamIndex,
		GradeScore:    req.GradeScore,
		CorrectCount:  req.CorrectCount,
		FaultCount:    req.FaultCount,
		TopicCount:    req.TopicCount,
		ExamSpendTime: req.ExamSpendTime,
	}

	id, err := service.GetGradeService().InsertGrade(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func UpdateGrade(ctx *engine.Context) (interface{}, error) {
	var req request.GradeUpdateRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.Grade{
		ID:            req.GradeID,
		UserID:        req.UserID,
		PaperID:       req.PaperID,
		ExamIndex:     req.ExamIndex,
		GradeScore:    req.GradeScore,
		CorrectCount:  req.CorrectCount,
		FaultCount:    req.FaultCount,
		TopicCount:    req.TopicCount,
		ExamSpendTime: req.ExamSpendTime,
	}

	id, err := service.GetGradeService().UpdateGrade(ctx, query)
	if err != nil {
		return nil, err
	}
	return id, nil
}

func DeleteGrade(ctx *engine.Context) (interface{}, error) {
	var req request.GradeQueryRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	query := model.Grade{
		ID: req.GradeID,
	}

	id, err := service.GetGradeService().DeleteGrade(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func GetGrade(ctx *engine.Context) (interface{}, error) {
	var req request.GradeQueryRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	query := model.Grade{
		ID:        req.GradeID,
		UserID:    req.UserID,
		PaperID:   req.PaperID,
		ExamIndex: req.ExamIndex,
	}

	grade, err := service.GetGradeService().GetGradeByID(ctx, query)
	if err != nil {
		return nil, err
	}
	return grade, nil
}

func GetGradeByPage(ctx *engine.Context) (interface{}, error) {
	var req request.GradeQueryRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	if err := ctx.BindQuery(&req.Page); err != nil {
		return nil, err
	}

	query := model.Grade{
		UserID:    req.UserID,
		PaperID:   req.PaperID,
		ExamIndex: req.ExamIndex,
	}

	list, count, err := service.GetGradeService().GetGradeByPage(ctx, query, &req.Page)
	if err != nil {
		return nil, nil
	}

	return response.ToPageResponse(list, req.PageIndex, req.PageSize, count), nil
}
