package handlers

import (
	"Panda/common/constant"
	"Panda/common/engine"
	"Panda/pojo/model"
	"Panda/pojo/request"
	"Panda/pojo/response"
	"Panda/service"
)

func InsertOption(ctx *engine.Context) (interface{}, error) {
	var req request.OptionInsertRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.Option{
		OptionText: req.OptionText,
		OptionKey:  req.OptionKey,
		QuestionID: req.QuestionID,
		CreatorID:  ctx.Session[constant.CtxCreatorID],
	}

	id, err := service.GetOptionService().InsertOption(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func DeleteOption(ctx *engine.Context) (interface{}, error) {
	var req request.OptionQueryRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	query := model.Option{
		ID:         req.OptionID,
		QuestionID: req.QuestionID,
		CreatorID:  ctx.Session[constant.CtxCreatorID],
	}

	id, err := service.GetOptionService().DeleteOption(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func UpdateOption(ctx *engine.Context) (interface{}, error) {
	var req request.OptionUpdateRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.Option{
		ID:         req.OptionID,
		OptionText: req.OptionText,
		OptionKey:  req.OptionKey,
		CreatorID:  ctx.Session[constant.CtxCreatorID],
	}

	id, err := service.GetOptionService().UpdateOption(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func GetOption(ctx *engine.Context) (interface{}, error) {
	var req request.OptionQueryRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	query := model.Option{
		ID:         req.OptionID,
		QuestionID: req.QuestionID,
		CreatorID:  ctx.Session[constant.CtxCreatorID],
	}

	option, err := service.GetOptionService().GetOption(ctx, query)
	if err != nil {
		return nil, err
	}

	return option, nil
}

func GetOptionByPage(ctx *engine.Context) (interface{}, error) {
	var req request.OptionQueryRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	if err := ctx.BindQuery(&req.Page); err != nil {
		return nil, err
	}

	query := model.Option{
		ID:         req.OptionID,
		QuestionID: req.QuestionID,
		CreatorID:  ctx.Session[constant.CtxCreatorID],
	}

	list, count, err := service.GetOptionService().GetOptionByPage(ctx, query, &req.Page)
	if err != nil {
		return nil, err
	}

	return response.ToPageResponse(list, req.PageIndex, req.PageSize, count), nil
}
