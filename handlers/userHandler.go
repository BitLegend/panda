package handlers

import (
	"Panda/common/constant"
	"Panda/common/engine"
	"Panda/common/exception"
	"Panda/pojo/model"
	"Panda/pojo/request"
	"Panda/pojo/response"
	"Panda/service"
	"strings"
)

// UserLogin 用户的统一登录处理器
func UserLogin(ctx *engine.Context) (interface{}, error) {
	req := request.UserLoginReq{}
	if err := ctx.BindJSON(&req); err != nil {
		return nil, exception.RequestParamsIllegal
	}
	query := &model.User{
		UserID:       strings.TrimSpace(req.UserID),
		UserPassword: strings.TrimSpace(req.UserPassword),
	}
	return service.GetUserService().UserLogin(ctx, query)
}

//UserRegister 用户统一进行注册的方法
func UserRegister(ctx *engine.Context) (interface{}, error) {
	req := request.UserRegisterReq{}
	if err := ctx.BindJSON(&req); err != nil {
		return nil, exception.RequestParamsIllegal
	}
	user := model.User{
		UserID:       req.UserID,
		UserPassword: req.UserPassword,
		UserMail:     req.UserMail,
		UserRole:     constant.RoleStudent,
	}
	nUser, err := service.GetUserService().UserRegister(ctx, user)
	if err != nil {
		return nil, err
	}
	resp := response.User{
		UserID:     nUser.UserID,
		UserName:   nUser.UserName,
		UserGender: nUser.UserGender,
		UserMail:   nUser.UserMail,
		UserRole:   nUser.UserRole,
	}
	return resp, nil
}

//GetUser 根据ID获取用户的信息
func GetUser(ctx *engine.Context) (interface{}, error) {
	req := request.UserQueryReq{}
	if err := ctx.BindQuery(req); err != nil {
		return nil, err
	}
	query := model.User{
		UserID: req.UserID,
	}
	user, err := service.GetUserService().GetUserByID(ctx, query)
	if err != nil {
		return nil, err
	}
	return user.ToResponse(), nil
}

// DeleteUserByID 根据ID删除用户的信息
func DeleteUserByID(ctx *engine.Context) (interface{}, error) {
	req := request.UserQueryReq{}
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}
	query := model.User{
		UserID: req.UserID,
	}
	id, err := service.GetUserService().DeleteUserByCond(ctx, query)
	if err != nil {
		return nil, err
	}
	return id, nil
}

// UpdateUser 根据ID更新的信息
func UpdateUser(ctx *engine.Context) (interface{}, error) {
	req := request.UserUpdateReq{}
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}
	if req.UserID == "" {
		return nil, exception.RequestParamsIllegal
	}
	query := model.User{
		UserID:      req.UserID,
		UserName:    req.UserName,
		UserGender:  constant.MatchGenderCode(req.UserGender),
		UserMail:    req.UserMail,
		UserTel:     req.UserTel,
		UserAddress: req.UserAddress,
		UserTeamID:  req.UserTeamID,
	}
	id, err := service.GetUserService().UpdateUserByID(ctx, query)
	if err != nil {
		return nil, err
	}
	return id, nil
}

//UpdateUserPwd 根据ID更新用户的密码信息
func UpdateUserPwd(ctx *engine.Context) (interface{}, error) {
	req := request.UserPwdReq{}
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	req.UserPassword = strings.TrimSpace(req.UserPassword)

	if req.UserPassword != req.CheckPassword || req.UserPassword == "" {
		return nil, exception.RequestParamsIllegal
	}

	query := model.User{
		UserID:       req.UserID,
		UserPassword: req.UserPassword,
	}
	id, err := service.GetUserService().UpdateUserPwd(ctx, query, req.OldPassword)
	if err != nil {
		return nil, err
	}

	return id, nil
}

//GetUserListPage 分页获取用户的列表信息 return page index,page count,line count
func GetUserListPage(ctx *engine.Context) (interface{}, error) {
	req := request.UserQueryReq{}
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	if err := ctx.BindQuery(req.Page); err != nil {
		return nil, err
	}

	if req.PageSize > 0 {
		if req.PageIndex <= 0 {
			req.PageIndex = 1
		}
		req.LineStart = (req.PageIndex - 1) * req.PageSize
	}

	query := model.User{
		UserID:     req.UserID,
		UserTeamID: req.UserTeamID,
	}
	list, count, err := service.GetUserService().GetUserListByPage(ctx, query, &req.Page)
	if err != nil {
		return nil, err
	}

	return response.ToPageResponse(list, req.PageIndex, req.PageSize, count), nil
}

func InsertUser(ctx *engine.Context) (interface{}, error) {
	var req request.UserRegisterReq
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.User{
		UserID:       req.UserID,
		UserPassword: req.UserPassword,
		UserMail:     req.UserMail,
		UserRole:     constant.RoleStudent,
	}
	user, err := service.GetUserService().UserRegister(ctx, query)
	if err != nil {
		return nil, err
	}

	resp := response.User{
		UserID:     user.UserID,
		UserName:   user.UserName,
		UserGender: user.UserGender,
		UserMail:   user.UserMail,
		UserTeamID: user.UserTeamID,
		UserRole:   user.UserRole,
	}
	return resp, nil
}
