package handlers

import (
	"Panda/common/constant"
	"Panda/common/engine"
	"Panda/pojo/model"
	"Panda/pojo/request"
	"Panda/pojo/response"
	"Panda/service"
)

func GetTeam(ctx *engine.Context) (interface{}, error) {
	var req request.TeamQueryRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	query := model.Team{
		ID: req.TeamID,
	}

	team, err := service.GetTeamService().GetTeamByID(ctx, query)
	if err != nil {
		return nil, err
	}

	return team, nil
}

func DeleteTeam(ctx *engine.Context) (interface{}, error) {
	var req request.TeamQueryRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	query := model.Team{
		ID: req.TeamID,
	}
	id, err := service.GetTeamService().DeleteTeam(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func UpdateTeam(ctx *engine.Context) (interface{}, error) {
	var req request.TeamUpdateRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.Team{
		ID:              req.TeamID,
		TeamName:        req.TeamName,
		TeamIntroduce:   req.TeamIntroduce,
		TeamMotto:       req.TeamMotto,
		TeamMemberCount: req.TeamMemberCount,
		TeamMail:        req.TeamMail,
		TeamAddress:     req.TeamAddress,
		TeamTel:         req.TeamTel,
		CreatorID:       ctx.Session[constant.CtxCreatorID],
	}

	id, err := service.GetTeamService().UpdateTeam(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func GetTeamByPage(ctx *engine.Context) (interface{}, error) {
	var req request.TeamQueryRequest
	if err := ctx.BindQuery(&req); err != nil {
		return nil, err
	}

	if err := ctx.BindQuery(&req.Page); err != nil {
		return nil, err
	}

	if req.PageSize > 0 {
		if req.PageIndex < 1 {
			req.PageIndex = 1
		}
		req.LineStart = (req.PageIndex - 1) * req.PageSize
	}

	query := model.Team{
		ID:        req.TeamID,
		TeamName:  req.TeamName,
		CreatorID: ctx.Session[constant.CtxCreatorID],
	}

	list, count, err := service.GetTeamService().GetTeamByCond(ctx, query, &req.Page)
	if err != nil {
		return nil, nil
	}

	return response.ToPageResponse(list, req.PageIndex, req.PageSize, count), nil
}

func InsertTeam(ctx *engine.Context) (interface{}, error) {
	var req request.TeamInsertRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.Team{
		TeamName:        req.TeamName,
		TeamIntroduce:   req.TeamIntroduce,
		TeamMotto:       req.TeamMotto,
		TeamMemberCount: req.TeamMemberCount,
		TeamMail:        req.TeamMail,
		TeamAddress:     req.TeamAddress,
		TeamTel:         req.TeamTel,
		CreatorID:       ctx.Session[constant.CtxCreatorID],
	}

	id, err := service.GetTeamService().InsertTeam(ctx, query)
	if err != nil {
		return nil, err
	}
	return id, nil
}
