package handlers

import (
	"Panda/common/engine"
	"Panda/pojo/model"
	"Panda/pojo/request"
	"Panda/pojo/response"
	"Panda/service"
)

func InsertExamRecord(ctx *engine.Context) (interface{}, error) {
	var req request.RecordQueryRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.ExamRecord{
		ID:          req.RecordID,
		PaperID:     req.PaperID,
		UserID:      req.UserID,
		ExamIndex:   req.ExamIndex,
		RecordScore: req.RecordScore,
	}

	id, err := service.GetExamRecordService().InsertExamRecord(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func UpdateExamRecord(ctx *engine.Context) (interface{}, error) {
	var req request.RecordUpdateRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.ExamRecord{
		ID:           req.RecordID,
		UserAnswer:   req.UserAnswer,
		ActualAnswer: req.ActualAnswer,
		RecordScore:  req.RecordScore,
	}

	id, err := service.GetExamRecordService().UpdateExamRecord(ctx, query)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func DeleteExamRecord(ctx *engine.Context) (interface{}, error) {
	var req request.RecordQueryRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.ExamRecord{
		ID: req.RecordID,
	}

	count, err := service.GetExamRecordService().DeleteExamRecord(ctx, query)
	if err != nil {
		return nil, err
	}
	return count, nil
}

func GetExamRecord(ctx *engine.Context) (interface{}, error) {
	var req request.RecordQueryRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	query := model.ExamRecord{
		ID: req.RecordID,
	}

	res, err := service.GetExamRecordService().GetExamRecord(ctx, query)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func GetExamRecordByPage(ctx *engine.Context) (interface{}, error) {
	var req request.RecordQueryRequest
	if err := ctx.BindJSON(&req); err != nil {
		return nil, err
	}

	if err := ctx.BindJSON(&req.Page); err != nil {
		return nil, err
	}

	query := model.ExamRecord{
		ID:          req.RecordID,
		PaperID:     req.PaperID,
		UserID:      req.UserID,
		ExamIndex:   req.ExamIndex,
		RecordScore: req.RecordScore,
	}

	list, count, err := service.GetExamRecordService().GetExamRecordByPage(ctx, query, &req.Page)
	if err != nil {
		return nil, err
	}
	return response.ToPageResponse(list, req.PageIndex, req.PageSize, count), nil
}
