package test

import (
	"sync"
	"testing"

	"github.com/smartystreets/goconvey/convey"
)

func TestSyncMap(t *testing.T) {
	convey.Convey("test map", t, func() {
		var syncMap sync.Map
		syncMap.Load("1")
		syncMap.Delete("1")
		syncMap.LoadAndDelete("1")
		syncMap.Range(func(key, value interface{}) bool {
			// return true the loop will continue
			return false
		})
		syncMap.Store("1", "1")
		syncMap.LoadOrStore("1", "1")
	})
}
