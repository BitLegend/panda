package main

import (
	"Panda/common/datasource"
	"Panda/common/setting"
	"Panda/module/inter"
	"fmt"

	"github.com/gin-gonic/gin"
)

func Init() {
	// init application global config
	if err := setting.InitAppConfig(); err != nil {
		panic(fmt.Sprintf("Panda init app config fail.[err=%v]", err))
	}
	// init db
	if err := datasource.InitDB(); err != nil {
		panic(fmt.Sprintf("Panda init datasource fail.[err=%v]", err))
	}
	// init service module
	inter.InitService()
	// init application global cache
	initCache()
}

func initCache() {

}

func main() {
	Init()
	// init application router group
	g := gin.New()
	InitRouter(g)
	// start server at 8080 port
	portAddress := fmt.Sprintf(":%v", setting.AppConfig.Application.GetPort())
	if err := g.Run(portAddress); err != nil {
		panic(err)
	}
}
