module Panda

go 1.15

require (
	4d63.com/gochecknoglobals v0.0.0-20210416044342-fb0abda3d9aa // indirect
	github.com/chavacava/garif v0.0.0-20210405164556-e8a0a408d6af // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/gin-gonic/gin v1.7.1
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/golangci/golangci-lint v1.39.0 // indirect
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/gostaticanalysis/analysisutil v0.7.1 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/julz/importas v0.0.0-20210405141620-a22c8f743dc9 // indirect
	github.com/magiconair/properties v1.8.5 // indirect
	github.com/mattn/go-runewidth v0.0.12 // indirect
	github.com/mgechev/revive v1.0.6 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/pelletier/go-toml v1.9.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/polyfloyd/go-errorlint v0.0.0-20210418123303-74da32850375 // indirect
	github.com/prometheus/common v0.10.0
	github.com/quasilyte/go-ruleguard v0.3.4 // indirect
	github.com/quasilyte/regex/syntax v0.0.0-20200805063351-8f842688393c // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/smartystreets/goconvey v1.6.4
	github.com/spf13/afero v1.6.0 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.7.1
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/tetafro/godot v1.4.5 // indirect
	github.com/tomarrell/wrapcheck v1.1.0 // indirect
	github.com/tommy-muehle/go-mnd/v2 v2.3.2 // indirect
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/sys v0.0.0-20210415045647-66c3f260301c // indirect
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
