package instance

import (
	"Panda/common/exception"
	"Panda/module"
)

func init() {
	module.RegisterSel("default", &DefaultSelector{})
}

type DefaultSelector struct {
}

// SelectQuestionList 直接生成一张试卷的全部题目信息
func (impl *DefaultSelector) SelectQuestionList(paperID int64, userID string) ([]int64, error) {
	return nil, nil
}

// SelectQuestion 根据当前的题目位置生成下次题目信息
func (impl *DefaultSelector) SelectQuestion(paperID, questionIndex int64, userID string) (int64, error) {
	if paperID == 0 || questionIndex < 0 || userID == "" {
		return 0, exception.RequestParamsIllegal
	}

	return 0, nil
}
