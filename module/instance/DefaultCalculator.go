package instance

import "Panda/module"

func init() {
	module.RegisterCal("default", &DefaultCalculator{})
}

type DefaultCalculator struct {
}

// ComputePaperScore 计算试卷的全部分数信息
func (impl *DefaultCalculator) ComputePaperScore(paperID int64, answers map[int64]string) (float64, error) {
	return 0, nil
}

// ComputeScore 计算单个题目的得分
func (impl *DefaultCalculator) ComputeScore(paperID int64, questionID int64, answer string) (float64, error) {
	return 0, nil
}
