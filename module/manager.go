package module

import "Panda/module/inter"

var (
	calculatorContainer map[string]inter.Calculator
	selectorContainer   map[string]inter.Selector
)

func RegisterCal(key string, calculator inter.Calculator) {
	if _, ok := calculatorContainer[key]; ok {
		return
	}
	calculatorContainer[key] = calculator
}

func RegisterSel(key string, selector inter.Selector) {
	if _, ok := selectorContainer[key]; ok {
		return
	}
	selectorContainer[key] = selector
}
