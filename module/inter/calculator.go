package inter

// Calculator 试卷分数计算接口
type Calculator interface {

	// ComputePaperScore 计算试卷的全部分数信息
	ComputePaperScore(paperID int64, answers map[int64]string) (float64, error)

	// ComputeScore 计算单个题目的得分
	ComputeScore(paperID int64, questionID int64, answer string) (float64, error)
}
