package inter

// Selector 题目选择器接口
type Selector interface {

	// SelectQuestionList 直接生成一张试卷的全部题目信息
	SelectQuestionList(paperID int64, userID string) ([]int64, error)

	// SelectQuestion 根据当前的题目位置生成下次题目信息
	SelectQuestion(paperID, questionIndex int64, userID string) (int64, error)
}
