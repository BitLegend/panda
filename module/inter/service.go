package inter

import (
	"Panda/common/datasource"
	"fmt"
	"sync"

	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

type Service interface {
	InitService(*gorm.DB)
}

var services = sync.Map{}

func RegisterService(key string, s Service) {
	old, has := services.Load(key)
	if has && old.(Service) == s {
		panic(errors.New(fmt.Sprintf("RegisterService duplicate [key=%v,v=%v]", key, s)))
	}
	services.Store(key, s)
}

func InitService() {
	services.Range(func(key, s interface{}) bool {
		s.(Service).InitService(datasource.GetConnection())
		return true
	})
}
